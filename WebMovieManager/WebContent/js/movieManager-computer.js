//Au démarrage, 
$(document).ready(function() {
	//Réglage des paramètres par défaut des requêtes AJAX
	$.ajaxSetup({
		beforeSend: function(XMLHttpRequest) {
			//On indique à l'utilisateur la progression avant la requête
			infoProgress();
		},
		timeout: 100000,
		error : infoErrorConnexion,
		success : function(html) {
			//On charge le contenu de la réponse dans la zone op
			loadPanel(html);
		}
	});
	
	//On charge la filmotheque
	loadMovieManager();
	
	//On adapte les listes à la bonne hauteur dès que la fenêtre est redimensionnée
	$(window).resize(function(){
		autoAdaptListHeight();
	});
	
});

/**
 * Adapte les listes à la bonne hauteur
 */
function autoAdaptListHeight() {
	var windowInnerHeight = window.innerHeight;
	var movieListHeight = windowInnerHeight - 105 - 96;
	$('.list').height(movieListHeight + 'px');
}

//////////////////////////
//FONCTIONS LISTE FILMS //
//////////////////////////

/**
 * Charge l'affichage de la filmotheque
 * @param html
 */
function loadMovieManager() {
	//On lance la requête
	$.get(
		URL_MOVIE_MANAGER_SERVLET + '?action=execInit',  
		function(html) {
			//On charge le contenu de la réponse dans la zone op
			loadPanel(html);
			//On adapte les listes à la bonne hauteur
			autoAdaptListHeight();
			// On initialise la popup pour le contenu des films
			$("#movieInfos").dialog({
				width: 600,
				autoOpen: false,
				show: "blind",
				hide: "explode",
				// Lorsqu'on ferme la popup
				close: function() {
					// On déselectionne le film éventuellement sélectionnés
					$('div.movie').removeClass('selected');
					// On vide le contenu du DOM de la popup
					$('#movieInfos').empty();
				}
			});
		}
	);
}

/**
 * Active le tri des films par genre
 */
function sortByGenre() {
	//On rétrécit la largeur de la présentation des films
	$('#listMovies').removeClass('searchList');
	//On sélectionne le bouton
	$('.btnSort').removeClass('selected');
	$('#btnSearch').removeClass('selected');
	$('#btnSortByGenre').addClass('selected');
	//On affiche la liste des genres et on cache les autres
	$('.listSorting').hide();
	$('#searchResultsTitle').hide();
	$('#listGenres').show();
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//Réinitialise le filtre
	filterByGenre('all');
}

/**
 * Active le tri des films par dossier
 */
function sortByFolder() {
	//On rétrécit la largeur de la présentation des films
	$('#listMovies').removeClass('searchList');
	//On sélectionne le bouton
	$('.btnSort').removeClass('selected');
	$('#btnSearch').removeClass('selected');
	$('#btnSortByFolder').addClass('selected');
	//On affiche la liste des dossiers et on cache les autres
	$('.listSorting').hide();
	$('#searchResultsTitle').hide();
	$('#listFolders').show();
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//Réinitialise le filtre
	filterByFolder('all');
}

/**
 * Active le tri des films par popularité
 */
function sortByPopularity() {
	//On rétrécit la largeur de la présentation des films
	$('#listMovies').removeClass('searchList');
	//On sélectionne le bouton
	$('.btnSort').removeClass('selected');
	$('#btnSearch').removeClass('selected');
	$('#btnSortByPopularity').addClass('selected');
	//On affiche la liste des dossiers et on cache les autres
	$('.listSorting').hide();
	$('#searchResultsTitle').hide();
	$('#listPopularity').show();
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//Réinitialise le filtre
	filterByPopularity('all');
}

/**
 * Filtre les films par genre
 * @param genre
 */
function filterByGenre(genre) {
	//On sélectionne le genre cliqué
	$('li.genre').removeClass('selected');
	$('#genre_' + genre).addClass('selected');
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//On filtre
	var movies = $('.movie', '#listMovies');
	movies.each(function() {
		var movie = $(this);
		if (movie.hasClass('genre_' + genre) || genre == "all") {
			movie.addClass('showMovie');
		} else {
			movie.removeClass('showMovie');
		}
	});
}

/**
 * Filtre les films par dossier
 * @param folder
 */
function filterByFolder(folder) {
	//On sélectionne le dossier cliqué
	$('li.folder').removeClass('selected');
	$('#folder_' + folder).addClass('selected');
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//On filtre
	var movies = $('.movie', '#listMovies');
	movies.each(function() {
		var movie = $(this);
		if (movie.hasClass('folder_' + folder) || folder == "all") {
			movie.addClass('showMovie');
		} else {
			movie.removeClass('showMovie');
		}
	});
}

/**
 * Filtre les films par popularité
 * @param folder
 */
function filterByPopularity(popularity) {
	//On sélectionne le dossier cliqué
	$('li.popularity').removeClass('selected');
	$('#popularity_' + popularity).addClass('selected');
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//On filtre
	var movies = $('.movie', '#listMovies');
	movies.each(function() {
		var movie = $(this);
		if (movie.hasClass('popularity_' + popularity) || popularity == "all") {
			movie.addClass('showMovie');
		} else {
			movie.removeClass('showMovie');
		}
	});
}

/**
 * Affiche tous les films
 */
function showAll() {
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//On sélectionne le bouton
	$('.btnShow').removeClass('selected');
	$('#btnShowAll').addClass('selected');
	//On enlève la classe sur la liste précisant qu'on ne veut que les films à voir
	$('#listMovies').removeClass('alreadySeen').removeClass('neverSeen').removeClass('toSee');
}

/**
 * Affiche uniquement les films à voir
 */
function showToSee() {
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//On sélectionne le bouton
	$('.btnShow').removeClass('selected');
	$('#btnShowToSee').addClass('selected');
	//On applique la classe sur la liste précisant qu'on ne veut que les films à voir
	$('#listMovies').removeClass('alreadySeen').removeClass('neverSeen').addClass('toSee');
}

/**
 * Affiche uniquement les films déjà vus
 */
function showAlreadySeen() {
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//On sélectionne le bouton
	$('.btnShow').removeClass('selected');
	$('#btnShowAlreadySeen').addClass('selected');
	//On applique la classe sur la liste précisant qu'on ne veut que les films à voir
	$('#listMovies').removeClass('toSee').removeClass('neverSeen').addClass('alreadySeen');
}

/**
 * Affiche uniquement les films jamais vus
 */
function showNeverSeen() {
	//On déselectionne les films éventuellement sélectionnés
	$('div.movie').removeClass('selected');
	//On masque les informations de film éventuellement affichées
	$('#movieInfos').empty().dialog("close");
	//On sélectionne le bouton
	$('.btnShow').removeClass('selected');
	$('#btnShowNeverSeen').addClass('selected');
	//On applique la classe sur la liste précisant qu'on ne veut que les films à voir
	$('#listMovies').removeClass('toSee').removeClass('alreadySeen').addClass('neverSeen');
}

/**
 * Affiche les films sous forme d'icônes
 */
function displayIcons() {
	//On applique la classe pour les icones
	$('#listMovies').removeClass("listStyle").addClass("iconStyle");
	//On sélectionne le bouton
	$('.btnDisplay').removeClass('selected');
	$('#btnDisplayIcons').addClass('selected');
}

/**
 * Affiche les films sous forme de liste
 */
function displayList() {
	//On applique la classe pour la liste
	$('#listMovies').removeClass("iconStyle").addClass("listStyle");
	//On sélectionne le bouton
	$('.btnDisplay').removeClass('selected');
	$('#btnDisplayList').addClass('selected');
}

/**
 * Charge l'affichage des informations d'un film
 * @param idMovie
 */
function loadMovieInfos(idMovie) {
	//On sélectionne le film cliqué
	$('div.movie').removeClass('selected');
	$('#movie_' + idMovie).addClass('selected');

	//On lance la requête
	$.get(
		URL_MOVIE_SERVLET + '?action=execDisplayInfos',
		{idMovie: idMovie},
		function(html) {
			//On charge le contenu de la réponse
			loadPanel(html, "movieInfos");
			//On affiche les notes sous forme graphique
			loadRatings($('#pressRatingValue').val(), $('#userRatingValue').val());
			//Si la dernière mise à jour date de plus de 10 jours, alors on met à jour les infos
			var nbDaysSinceLastUpdate = $('#nbDaysSinceLastUpdate').val();
			if (nbDaysSinceLastUpdate > 1) {
				updateMovieInfos(idMovie);
			}
			// On affiche la popup
			$("#movieInfos").dialog("open");
			return false;
		}
	);
}

/**
 * Met à jour les infos d'un film depuis AlloCine
 * @param idMovie
 */
function updateMovieInfos(idMovie) {
	//On lance la requête
	$.getJSON(
		URL_MOVIE_SERVLET + '?action=execUpdateInfos',
		{idMovie: idMovie},
		function(jsonObject) {
			//Cachage du message de chargement
			infoHide();
			//On met à jour les critiques
			loadRatings(jsonObject.pressRating, jsonObject.userRating);
			// On met éventuellement à jour le poster
			if (jsonObject.hasNewPoster == true) {
				$('#posterMovieManager_' + idMovie).attr("src", "/WebMovieManager/Auth/Movie?action=execDisplayPosterThumbnail&idMovie=" + idMovie);
				$('#posterMovieInfos_' + idMovie).attr("src", "/WebMovieManager/Auth/Movie?action=execDisplayPosterOriginal&idMovie=" + idMovie).addClass('poster');
			}
		}
	);
}

/**
 * Met à jour les infos de tous les films
 * @param idMovie
 */
function updateAll() {
	//On lance la requête
	$.post(
		URL_MOVIE_MANAGER_SERVLET + '?action=execUpdateAllInfos',
		{},
		function() {
			//Cachage du message de chargement
			infoHide();
		}
	);
}


/**
 * Affiche les notes sous forme graphique
 * @param pressRating
 * @param userRating
 */
function loadRatings(pressRating, userRating) {
	var pressRatingValue = new Number(pressRating);
	var userRatingValue = new Number(userRating);
	// Efface les étoiles éventuellement déjà présentes
	$('#pressRatingContainer').removeClass("rating0").removeClass("rating05").removeClass("rating1").removeClass("rating15").removeClass("rating2").removeClass("rating25").removeClass("rating3").removeClass("rating35").removeClass("rating4").removeClass("rating45").removeClass("rating5");
	$('#userRating').removeClass("rating0").removeClass("rating05").removeClass("rating1").removeClass("rating15").removeClass("rating2").removeClass("rating25").removeClass("rating3").removeClass("rating35").removeClass("rating4").removeClass("rating45").removeClass("rating5");
	// Affiche ou non la zone
	if (pressRatingValue == 0) {
		$('#pressRating').hide();
	} else {
		$('#pressRating').show();
	}
	if (userRatingValue == 0) {
		$('#userRating').hide();
	} else {
		$('#userRating').show();
	}
	// Charge les étoiles
	if (pressRatingValue == 0) {
		$('#pressRatingContainer').addClass("rating0");
	} else if (pressRatingValue <= 0.5) {
		$('#pressRatingContainer').addClass("rating05");
	} else if (pressRatingValue <= 1) {
		$('#pressRatingContainer').addClass("rating1");
	} else if (pressRatingValue <= 1.5) {
		$('#pressRatingContainer').addClass("rating15");
	} else if (pressRatingValue <= 2) {
		$('#pressRatingContainer').addClass("rating2");
	} else if (pressRatingValue <= 2.5) {
		$('#pressRatingContainer').addClass("rating25");
	} else if (pressRatingValue <= 3) {
		$('#pressRatingContainer').addClass("rating3");
	} else if (pressRatingValue <= 3.5) {
		$('#pressRatingContainer').addClass("rating35");
	} else if (pressRatingValue <= 4) {
		$('#pressRatingContainer').addClass("rating4");
	} else if (pressRatingValue <= 4.5) {
		$('#pressRatingContainer').addClass("rating45");
	} else {
		$('#pressRatingContainer').addClass("rating5");
	}
	if (userRatingValue == 0) {
		$('#userRatingContainer').addClass("rating0");
	} else if (userRatingValue <= 0.5) {
		$('#userRatingContainer').addClass("rating05");
	} else if (userRatingValue <= 1) {
		$('#userRatingContainer').addClass("rating1");
	} else if (userRatingValue <= 1.5) {
		$('#userRatingContainer').addClass("rating15");
	} else if (userRatingValue <= 2) {
		$('#userRatingContainer').addClass("rating2");
	} else if (userRatingValue <= 2.5) {
		$('#userRatingContainer').addClass("rating25");
	} else if (userRatingValue <= 3) {
		$('#userRatingContainer').addClass("rating3");
	} else if (userRatingValue <= 3.5) {
		$('#userRatingContainer').addClass("rating35");
	} else if (userRatingValue <= 4) {
		$('#userRatingContainer').addClass("rating4");
	} else if (userRatingValue <= 4.5) {
		$('#userRatingContainer').addClass("rating4.5");
	} else {
		$('#userRatingContainer').addClass("rating5");
	}
}

/**
 * Recherche un film
 */
function searchMovies() {
	//On lance la requête
	var pattern = $('#searchField').val();
	$.post(
		URL_MOVIE_MANAGER_SERVLET + '?action=execSearchMovies',
		{q: pattern},
		function(jsonObject) {
			//Cachage du message de chargement
			infoHide();
			//On sélectionne le bouton
			$('.btnSort').removeClass('selected');
			$('#btnSearch').addClass('selected');
			//On masque les listes de tris
			$('.listSorting').hide();
			$('#searchResultsTitle').show();
			//On affiche le pattern recherché
			$('#searchPattern').html(pattern);
			//On étend la largeur de la présentation des films
			$('#listMovies').addClass('searchList');
			//Tout d'abord, on masque tous les films
			var movies = $('.movie', '#listMovies');
			movies.each(function() {
				$(this).removeClass('showMovie');
			});
			if (jsonObject.movies != null) {
				//Il y a des résultats : on affiche les films trouvés
				var idMovies = jsonObject.movies;
				$(idMovies).each(function() {
					$('#movie_' + this).addClass('showMovie');
				});
			}
		},
		'json'
	);
}

//////////////////////////////
//FONCTIONS SYNCHRONISATION //
//////////////////////////////

/**
 * Lance la synchronisation
 */
function synchro() {
	//On lance la requête
	$.post(URL_MOVIE_MANAGER_SERVLET + '?action=execSynchro');
};

/**
 * Lance la récupération des informations d'un film sur AlloCine
 * @param idMovie
 */
function selectMovieInfos(idMovie) {
	//On récupère l'url du fichier à renseigner
	var urlFile = $('#urlFile').val().replace(/&/g,"%26");
	// Vérification des marqueurs
	var toSee = $('#toSee').hasClass('selected');
	var alreadySeen = $('#alreadySeen').hasClass('selected');
	if (!toSee && !alreadySeen) {
		if (!confirm("Attention aucun marqueur sélectionné. Continuer ?")) {
			return;
		}
	}
	//On lance la requête
	$.post(
		URL_SEARCH_INFOS_SERVLET + '?action=execValidateInfos',
		{idMovie: idMovie, urlFile: urlFile, toSee: toSee, alreadySeen: alreadySeen}
	);
}

/**
 * Lance le signalement du fichier comme n'étant pas un film
 */
function selectNotAMovie() {
	if (confirm("Ceci signalera le fichier comme n'étant pas un film. Etes-vous sûr de vouloir faire ceci ?")) {
		//On récupère l'url du fichier à renseigner
		var urlFile = $('#urlFile').val().replace(/&/g,"%26");
		//On lance la requête
		$.post(
			URL_SEARCH_INFOS_SERVLET + '?action=execReportNotAMovie',
			{urlFile: urlFile}
		);
	}
}

/**
 * Modifie le nom à rechercher si les résultats de recherche ne sont pas satisfaisants
 */
function editSearchName() {
	$('#searchName').show();
	$('#btnSearchName').show();
}

/**
 * Lance la recherche d'un film avec un nom choisi par l'utilisateur
 */
function searchNewName() {
	//On récupère l'url du fichier à renseigner
	var urlFile = $('#urlFile').val().replace(/&/g,"%26");
	//On récupère le nom à rechercher
	var searchName = $('#searchName').val();
	//On lance la requête
	$.post(
		URL_SEARCH_INFOS_SERVLET + '?action=execSearchInfos',
		{nameMovieToSearch: searchName, urlFile: urlFile}
	);
}

/**
 * Renseigne l'identifiant manuellement si les résultats de recherche ne sont pas satisfaisants
 */
function enterIdMovieManually() {
	$('#idMovie').show();
	$('#btnValidateId').show();
}

/**
 * Lance la validation de l'identifiant d'un film renseigné manuellement
 */
function validateManualIdMovie() {
	//On récupère le nom à rechercher
	var idMovie = $('#idMovie').val();
	//Demande de confirmation
	if (confirm("Etes-vous sûr de vouloir renseigner l'identifiant '" + idMovie + "' pour ce film ?")) {
		//On lance la requête
		selectMovieInfos(idMovie);
	}
}

/**
 * Lance la watchMovie d'un film
 */
function watchMovie() {
	//On récupère l'url du fichier
	var urlFile = $('#urlFile').val().replace(/&/g,"%26");
	//On lance la requête
	$(location).attr('href', URL_MOVIE_SERVLET + '?action=execWatchMovie&urlFile=' + urlFile);
}

/**
 * Marque un film comme à voir (ou vice versa)
 * @param idMovie
 */
function setToSee(idMovie) {
	//On récupère l'url du fichier
	var toSee = $('#movieToSee_' + idMovie).val();
	if (toSee != "true" && toSee != "false") {
		return false;
	}
	//On lance la requête
	var alreadySeenNew;
	if (toSee == "false") {
		alreadySeenNew = true;
	} else {
		alreadySeenNew = false;
	}
	$.post(
		URL_MOVIE_SERVLET + '?action=execSetToSee',
		{idMovie: idMovie, toSee: alreadySeenNew},
		function(html) {
			infoHide();
			//On regarde si la réponse est une exception
			var exception = $('exception', html);
			if (exception.length != 0) {
				//Si c'est une exception, on l'affiche
				msg = exception[0].textContent;
				infoError(msg);
				return
			}
			// Changement de la valeur du champ caché et de la classe CSS
			$('#movieToSee_' + idMovie).val(''+alreadySeenNew);
			if (alreadySeenNew) {
				$('#toSee_' + idMovie).addClass('selected');
				$('#movie_' + idMovie).addClass('toSee');
			} else {
				$('#toSee_' + idMovie).removeClass('selected');
				$('#movie_' + idMovie).removeClass('toSee');
			}
		}
	);
}

/**
 * Marque un film comme déjà vu (ou vice versa)
 * @param idMovie
 */
function setAlreadySeen(idMovie) {
	//On récupère l'url du fichier
	var alreadySeen = $('#movieAlreadySeen_' + idMovie).val();
	if (alreadySeen != "true" && alreadySeen != "false") {
		return false;
	}
	//On lance la requête
	var alreadySeenNew;
	if (alreadySeen == "false") {
		alreadySeenNew = true;
	} else {
		alreadySeenNew = false;
	}
	$.post(
		URL_MOVIE_SERVLET + '?action=execSetAlreadySeen',
		{idMovie: idMovie, alreadySeen: alreadySeenNew},
		function(html) {
			infoHide();
			//On regarde si la réponse est une exception
			var exception = $('exception', html);
			if (exception.length != 0) {
				//Si c'est une exception, on l'affiche
				msg = exception[0].textContent;
				infoError(msg);
				return
			}
			// Changement de la valeur du champ caché et de la classe CSS
			$('#movieAlreadySeen_' + idMovie).val(''+alreadySeenNew);
			if (alreadySeenNew) {
				$('#alreadySeen_' + idMovie).addClass('selected');
				$('#movie_' + idMovie).removeClass('neverSeen').addClass('alreadySeen');
			} else {
				$('#alreadySeen_' + idMovie).removeClass('selected');
				$('#movie_' + idMovie).removeClass('alreadySeen').addClass('neverSeen');
			}
		}
	);
}

/**
 * Supprime un film de la filmotheque
 */
function deleteFromMovieManager() {
	//Demande de confirmation
	if (confirm("Ceci supprimera le film de la filmothèque. Etes-vous sûr de vouloir faire ceci ?")) {
		//On récupère l'url du fichier
		var idMovie = $('#idMovie').val();
		//On lance la requête
		$.post(
			URL_MOVIE_SERVLET + '?action=execDeleteFromMovieManager',
			{idMovie: idMovie}
		);
	}
}

/**
 * Supprime un film du disque
 */
function deleteFromDisk() {
	if (confirm("Ceci supprimera le film du disque. Etes-vous sûr de vouloir faire ceci ?")) {
		if (confirm("ATTENTION : CETTE ACTION EST IRREVERSIBLE. Etes-vous sûr de vouloir faire ceci ?")) {
			//On récupère l'url du fichier
			var idMovie = $('#idMovie').val();
			//On lance la requête
			$.post(URL_MOVIE_SERVLET + '?action=execDeleteFromDisk',
					{idMovie: idMovie}
			);
		}
	}
}

////////////////////////////
//FONCTIONS CONFIGURATION //
////////////////////////////

/**
 * Affiche les réglages
 */
function loadConfiguration() {
	//On lance la requête
	$.get(URL_CONFIGURATION_SERVLET + '?action=execInit');
}

/**
 * Cochage/décochage de la case "proxy"
 */
function selectProxy() {
	if ($('input[name=networkProxy]').attr("checked") == true) {
		$('input[name=networkProxyHost]').removeAttr("disabled");
		$('input[name=networkProxyPort]').removeAttr("disabled");
		$('input[name=networkProxyLogin]').removeAttr("disabled");
		$('input[name=networkProxyPassword]').removeAttr("disabled");
	} else {
		$('input[name=networkProxyHost]').attr("disabled","true");
		$('input[name=networkProxyPort]').attr("disabled","true");
		$('input[name=networkProxyLogin]').attr("disabled","true");
		$('input[name=networkProxyPassword]').attr("disabled","true");
	}
}

/**
 * Enregistre les réglages
 * @param html
 */
function saveConfiguration() {
	//On récupère les informations de configuration
	var params = $('#formConfig').serialize();
	//On lance la requête
	$.post(
		URL_CONFIGURATION_SERVLET + '?action=execSave',
		params);
}

//////////////////////////
// FONCTIONS GENERIQUES //
//////////////////////////


/**
 * Charge du contenu html dans la zone opérationnelle
 * @param html Contenu de la réponse HTML
 * @param divId Identifiant du bloc à remplir
 */
function loadPanel(html, divId) {
	//On regarde si la réponse est une exception
	var exception = $('exception', html);
	if (exception.length != 0) {
		//Si c'est une exception, on l'affiche
		msg = exception[0].textContent;
		infoError(msg);
		return
	}
	
	//Par défaut, le div à remplir est la zone opérationnelle
	if (divId == null) {
		divId = "content";
	}
	//On indique que l'onglet est chargé, et on remplit la zone opérationnelle
	$('#' + divId).html(html);
	//Cachage du message de chargement
	infoHide();
}

/**
 * Indique la progression dans la barre d'information
 */
function infoProgress() {
	$('#zoneInfoMsg').addClass('zoneInfo_valid').removeClass('zoneInfo_error');
	$('#zoneInfoMsg').html("Connexion...").show();
}

/**
 * Indique une erreur de connexion
 */
function infoErrorConnexion() {
	infoError("Erreur de connexion");
}

/**
 * Indique un message d'erreur
 * @param msg Message
 */
function infoError(msg) {
	$('#zoneInfoMsg').addClass('zoneInfo_error').removeClass('zoneInfo_valid');
	$('#zoneInfoMsg').html(msg);
	infoFadeSlow();
}

/**
 * Indique un message de succès
 * @param msg Message
 */
function infoSuccess(msg) {
	$('#zoneInfoMsg').addClass('zoneInfo_valid').removeClass('zoneInfo_error');
	$('#zoneInfoMsg').html(msg);
	infoFade();
}

/**
 * Effet de fondue sur le message d'information
 */
function infoFade()
{
	$('#zoneInfoMsg').fadeIn("slow");
	setTimeout(function() {$('#zoneInfoMsg').fadeOut("slow");} , 2000);
}

/**
 * Effet de fondue lent (pour les messages d'erreur)
 */
function infoFadeSlow()
{
	$('#zoneInfoMsg').fadeIn("slow");
	setTimeout(function() {$('#zoneInfoMsg').fadeOut("slow");} , 7000);
}

/**
 * Cache le message d'information
 */
function infoHide()
{
	$('#zoneInfoMsg').hide();
}
