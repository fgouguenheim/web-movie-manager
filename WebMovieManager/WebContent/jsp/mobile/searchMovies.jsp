<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@page import="java.util.Iterator"%>
<%@ page import="com.fgougui.webmoviemanager.beans.MovieBean" %>

<jsp:useBean id="movies" type="java.util.Collection" scope="request"/>

<!-- Liste des résultats -->
<% if (movies.isEmpty()) { %>
	<h2>Aucun r&eacute;sultat</h2>
<% } else { %>
	<h2>Choix du r&eacute;sultat</h2>
	<ul class="rounded">
		<% Iterator<MovieBean> it = movies.iterator();
		while (it.hasNext()) {
			MovieBean movie = it.next(); %>
			<li id="movie_<%= movie.getId() %>" class="arrow movie <% for(String genre : movie.getGenres()) { %> genre_<%= genre.replaceAll(" ", "_") %><% } %> folder_<%=movie.getFolder().replaceAll(" ", "_")%>"
				onclick="loadMovieInfos(<%= movie.getId() %>)">
				<span><%=movie.getTitle()%></span>
			</li>
		<% } %>
	</ul>
<% } %>