<%@page import="org.joda.time.DateTime"%>
<%@page import="org.joda.time.Days"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@page import="java.util.Set"%>
<%@page import="com.fgougui.webmoviemanager.beans.ActorBean"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Date"%>

<jsp:useBean id="movie" class="com.fgougui.webmoviemanager.beans.MovieBean" scope="request"/>

<!-- Titre (& éventuel titre original) -->
<h3><%=movie.getTitle()%></h3>

<!-- Champs cachés -->
<input type="hidden" id="urlTrailer" value="<%=movie.getUrlTrailer()%>"/>
<input type="hidden" id="idMovie" value="<%=movie.getId()%>"/>
<input type="hidden" id="nbDaysSinceLastUpdate" value="<%= Days.daysBetween(new DateTime(movie.getLastUpdate()).withTimeAtStartOfDay(), DateTime.now().withTimeAtStartOfDay()).getDays() %>"/>

<!-- Infos du film -->
<div id="poster">
	<!-- Jaquette -->
	<%
		if (movie.getUrlPosterOriginal() != null) {
	%>
		<img alt="poster" src="/WebMovieManager/Auth/Movie?action=execDisplayPosterOriginal&idMovie=<%=movie.getId()%>" id="poster">
	<%
		} else {
	%>
		<img alt="poster" src="../images/mobile/nopicture.jpg">
	<%
		}
	%>
</div>
<div id="movieDetails">
	<!-- Réalisateur (éventuel) -->
	<div>
		<%
			if (StringUtils.isNotBlank(movie.getDirector())) {
		%>
			<b>de</b> <%=movie.getDirector()%>
		<%
			}
		%>
	<!-- Année de production -->
	(<%=movie.getYear()%>)
	</div>
	<!-- Casting (éventuel) -->
	<div>
		<%
			if (movie.getCasting() != null && !(movie.getCasting().isEmpty())) {
		%>
			<b>avec</b>
			<%
				Iterator<ActorBean> it = movie.getCasting().iterator();
					while (it.hasNext()) { 
				ActorBean acteur = it.next();
			%>
				<%=acteur.getName()%>
				<% if (StringUtils.isNotBlank(acteur.getRole())) { %>
					(<%= acteur.getRole() %>)<% if (it.hasNext()) { %>,<% } %>
				<% } %>
			<% } %>
		<% } %>
	</div>
	<!-- Notes presse/spectateurs -->
	<div>
		<div id="pressRating" style="display: hidden">
			<b>Note presse : </b>
			<img id="pressRatingContainer" class="ratingContainer" src="./images/mobile/empty.gif"/>
			<input type="hidden" id="pressRatingValue" value="<%= movie.getPressRating() %>"/>
		</div>
		<div id="userRating" style="display: hidden">
			<b>Note spectateurs : </b>
			<img id="userRatingContainer" class="ratingContainer" src="./images/mobile/empty.gif"/>
			<input type="hidden" id="userRatingValue" value="<%= movie.getUserRating() %>"/>
		</div>
	</div>
	<!-- Informations complémentaires -->
	<div>
		<% if (StringUtils.isNotBlank(movie.getOtherInfos())) { %>
			<div><i><%= movie.getOtherInfos() %></i></div>
		<% } %>
	</div>
	<!-- Genres -->
	<div>
		<b>Genre<% if (movie.getGenres().size() > 1) { %>s<% } %> : </b>
		<%= movie.getGenres().toString().substring(1, movie.getGenres().toString().length() - 1) %>
	</div>
	<!-- Nationalités -->
	<div>
		<b>Nationalit&eacute;<% if (movie.getNationalities().size() > 1) { %>s<% } %> : </b>
		<%= movie.getNationalities().toString().substring(1, movie.getNationalities().toString().length() - 1) %>
	</div>
	<!-- Synopsis -->
	<div>
		<b>Synopsis : </b>
		<%= movie.getSynopsis() %>
	</div>
	<!-- Lien vers la page du film sur AlloCine -->
	<div>
		<a href="#" onclick="window.open('http://iphone.allocine.fr/film/fichefilm_gen_cfilm=' + $('#idMovie').val() + '.html');"><b>Page film sur AlloCine</b></a>
	</div>
	<!-- Lien vers la bande annonce -->
	<div>
		<a href="#" onclick="window.open($('#urlTrailer').val());"><b>Bande annonce</b></a>
	</div>
	<!-- Liens de suppression -->
	<div>
		<a href="#" onclick="deleteFromMovieManager()"><b>Supprimer de la filmotheque</b></a>
	</div>
	<div>
		<a href="#" onclick="deleteFromDisk()"><b>Supprimer du disque</b></a>
	</div>
</div>