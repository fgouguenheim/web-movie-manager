<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:useBean id="movieManager" class="com.fgougui.webmoviemanager.beans.MovieManagerBean" scope="application"/>
<%@ page import="com.fgougui.webmoviemanager.beans.MovieBean" %>

<html>
  <head>
  	<!-- JQtouch -->
    <script src="/WebMovieManager/js/jquery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="/WebMovieManager/js/jquery/jqtouch/jqtouch/jqtouch.js" type="application/x-javascript" charset="utf-8"></script>
    <style type="text/css" media="screen">@import "/WebMovieManager/js/jquery/jqtouch/jqtouch/jqtouch.css";</style>
    <style type="text/css" media="screen">@import "/WebMovieManager/js/jquery/jqtouch/themes/default/theme.css";</style>
    <style type="text/css" media="screen">@import "/WebMovieManager/css/mobile/movieManager.css";</style>
    
    <!-- Script de la page -->
    <script src="/WebMovieManager/js/base.js" type="text/javascript"></script>
    <script src="/WebMovieManager/js/services.js" type="text/javascript"></script>
    <script src="/WebMovieManager/js/movieManager-mobile.js" type="text/javascript"></script>
    
    <!-- Titre -->
  	<title>WebMovieManager</title>
  </head>
  
  <body>
  
	  <div id="home" class="current">
	      <div class="toolbar">
	          <h1>Filmoth&egrave;que</h1>
	          <a class="button pop" href="#search">Recherche</a>
	      </div>
	      <h2>Choix du tri</h2>
	      <ul class="rounded">
	          <li class="arrow"><a class="swap" href="#listFolders">Tri par dossiers</a></li>
	          <li class="arrow"><a class="swap" href="#listGenres">Tri par genres</a></li>
	      </ul>
	  </div>
	  
	<!-- Liste des genres -->
	<div id="listGenres">
	    <div class="toolbar">
	        <h1>Genres</h1>
	        <a class="back" href="#">Tri</a>
	        <a class="button pop" href="#search">Recherche</a>
	    </div>
	    <%--<h2>Choix du genre</h2>--%>
	    <ul class="rounded">
	    	<li id="genre_all" class="arrow genre" onclick="filterByGenre('all')">
				<span>tous</span> <small class="counter"><%=movieManager.getMovies().size()%></small>
			</li>
	        <%
	        	for (String genre : movieManager.getGenres().keySet()) {
	        %>
				<li id="genre_<%=genre.replaceAll(" ", "_")%>" class="arrow genre" onclick="filterByGenre('<%=genre.replaceAll(" ", "_")%>')">
					<span><%=genre%></span> <small class="counter"><%=movieManager.getGenres().get(genre)%></small>
				</li>
			<%
				}
			%>
		</ul>
	</div>
	
	<!-- Liste des dossiers -->
	<div id="listFolders">
	    <div class="toolbar">
	        <h1>Dossiers</h1>
	        <a class="back" href="#">Tri</a>
	        <a class="button pop" href="#search">Recherche</a>
	    </div>
	    <%--<h2>Choix du dossier</h2>--%>
	    <ul class="rounded">
	    	<li id="folder_all" class="arrow folder" onclick="filterByFolder('all')">
	    		<span>tous</span> <small class="counter"><%=movieManager.getMovies().size()%></small>
	    	</li>
	    	<%
	    		for (String folder : movieManager.getFolders().keySet()) {
	    	%>
				<li id="folder_<%=folder.replaceAll(" ", "_")%>" class="arrow folder" onclick="filterByFolder('<%=folder.replaceAll(" ", "_")%>')">
					<span><%= folder %></span> <small class="counter"><%=movieManager.getFolders().get(folder)%></small>
				</li>
			<% } %>
	    </ul>
	</div>
	
	<!-- Liste des films -->
	<div id="listMovies">
		<div class="toolbar">
	        <h1>Films</h1>
	        <a id="btn_retour_listMovies" class="back" href="#"></a>
	        <a class="button pop" href="#search">Recherche</a>
	    </div>
	    <h2 id="titleListMovies"></h2>
	    <div id="listMoviesAjax">
	    </div>
	</div>
	
	<!-- Recherche -->
	<div id="search">
		<div class="toolbar">
	        <h1>Recherche</h1>
	        <a class="goback button" href="#">Liste</a>
	    </div>
        <ul class="rounded">
            <li><input type="text" id="searchField" value="" placeholder="Recherche" /></li>
        </ul>
        <a style="margin:0 10px;color:rgba(0,0,0,.9)" href="#" class="submit whiteButton" onclick="searchMovies();">Rechercher</a>
	</div>
	
	<!-- Liste des résultats de recherche -->
	<div id="listMoviesSearch">
		<div class="toolbar">
	        <h1>R&eacute;sultats</h1>
	        <a class="back" href="#">Recherche</a>
	    </div>
	    <div id="listMoviesSearchAjax">
	    </div>
	</div>
	
	<!-- Infos d'un film -->
	<div id="movieInfos">
		<div class="toolbar">
	        <h1>Film</h1>
	        <a id="btn_retour_movieInfos" class="back" href="#">Liste</a>
	    </div>
	    <div id="movieInfosAjax">
	    </div>
	</div>
	
	
  </body>
</html>