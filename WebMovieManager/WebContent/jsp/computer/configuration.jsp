<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@page import="com.fgougui.webmoviemanager.exceptions.ConfigurationException"%>

<%-- Attention : utiliser type="" au lieu de name="" car il n'y a pas de constructeur
	 public dans la classe ConfigProperties --%>
<jsp:useBean id="configuration" type="com.fgougui.webmoviemanager.config.ConfigProperties" scope="request"/>

<!-- Titre -->
<h2>Configuration de l'application</h2>

<% 
//Si les paramètes obligatoires ne sont pas définis, une exception est lancée
//On doit ici catcher l'exception pour permettre à la page de s'afficher !
String movieManagerFilesPath = "";
String movieManagerFilesExtensionIgnore = "";
String dataFilesPath = "";
try {
	dataFilesPath = configuration.getDataFilesPath();
} catch (ConfigurationException e) {}
try {
	movieManagerFilesPath = configuration.getMovieManagerFilesPathCommaSeparated();
} catch (ConfigurationException e) {}
try {
	movieManagerFilesExtensionIgnore = configuration.getMovieManagerFilesExtensionIgnoreCommaSeparated();
} catch (ConfigurationException e) {}
%>


<form id="formConfig">

	<h3>Param&egrave;tres stockage</h3>
	<div class="formTable">
	    <div class="formLine">
	        <div class="formLabel"><b>R&eacute;pertoire de stockage des donn&eacute;es</b></div>
	        <div class="formHelp" title="Chemin du répertoire stockant les données de l'application. Si ce répertoire n'existe pas il sera créé. A renseigner en premier !"/>
	        <div class="formInput"><input type="text" class="xlarge" name="dataFilesPath" value="<%= dataFilesPath %>"/></div>
	    </div>
	    <div class="formLine">
	        <div class="formLabel">Mode sauvegarde</div>
	        <div class="formHelp" title="Sauvegarde les données à chaque enregistrement. Attention prend beaucoup de place !"/>
	        <div class="formInput"><input type="checkbox" name="dataBackup" <% if(configuration.isDataBackup()) { %>checked<% } %>/></div>
	    </div>
	</div>

	<h3>Param&egrave;tres filmoth&egrave;que</h3>
	<div class="formTable">
	    <div class="formLine">
	        <div class="formLabel">R&eacute;pertoire contenant les films</div>
	        <div class="formHelp" title="Chemins des répertoires racines de la filmothèque (séparés par une virgule)"/>
	        <div class="formInput"><input type="text" class="xlarge" name="movieManagerFilesPath" value="<%= movieManagerFilesPath %>"/></div>
	    </div>
	    <div class="formLine">
	        <div class="formLabel">Extensions de fichiers à ignorer</div>
	        <div class="formHelp" title="Extensions de fichiers qui ne sont jamais des films (séparés par une virgule)"/>
	        <div class="formInput"><input type="text" class="xlarge" name="movieManagerFilesExtensionIgnore" value="<%= movieManagerFilesExtensionIgnore %>"/></div>
	    </div>
	    <div class="formLine">
	        <div class="formLabel">Renommage automatique des fichiers</div>
	        <div class="formHelp" title="Renomme automatiquement les film à partir de leur titre"/>
	        <div class="formInput"><input type="checkbox" name="movieManagerAutoRename" <%if(configuration.isMovieManagerAutoRename()) {%>checked<% } %>/></div>
	    </div>
	</div>

	<h3>Param&egrave;tres r&eacute;seau</h3>
	<div class="formTable">
	    <div class="formLine">
	        <div class="formLabel">Proxy</div>
	        <div class="formHelp" title="Le serveur est-il derrière un proxy ?"/>
	        <div class="formInput"><input type="checkbox" name="networkProxy" <% if(configuration.isNetworkProxy()) { %>checked<% } %> onclick="selectProxy()"/></div>
	    </div>
	    <div class="formLine">
	        <div class="formLabel">H&ocirc;te</div>
	        <div class="formHelp" title="Hote du proxy"/>
	        <div class="formInput"><input type="text" class="large" name="networkProxyHost" value="<%= configuration.getNetworkProxyHost() %>"/></div>
	    </div>
	    <div class="formLine">
	        <div class="formLabel">Port</div>
	        <div class="formHelp" title="Port du proxy"/>
	        <div class="formInput"><input type="text" class="xsmall" name="networkProxyPort" value="<%= configuration.getNetworkProxyPort() %>"/></div>
	    </div>
	    <div class="formLine">
	        <div class="formLabel">Login</div>
	        <div class="formHelp" title="Login pour le proxy"/>
	        <div class="formInput"><input type="text" class="small" name="networkProxyLogin" value="<%= configuration.getNetworkProxyLogin() %>"/></div>
	    </div>
	    <div class="formLine">
	        <div class="formLabel">Password</div>
	        <div class="formHelp" title="Mot de passe pour le proxy"/>
	        <div class="formInput"><input type="text" class="small" name="networkProxyPassword" value="<%= configuration.getNetworkProxyPassword() %>"/></div>
	    </div>
	</div>
</form>

<input type="button" value="Enregistrer les modifications" class="button" onclick="saveConfiguration()"/>