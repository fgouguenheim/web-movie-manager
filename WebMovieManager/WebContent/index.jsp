<%@page import="com.fgougui.webmoviemanager.servlets.BaseMovieManagerServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="org.apache.commons.lang3.StringUtils"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Script de la page -->
	<script type="text/javascript" src="./js/base.js"></script>
	<!-- Titre -->
	<title>WebMovieManager</title>
</head>
	<body>
		<%
		    String userAgent = request.getHeader("USER-AGENT").toLowerCase();
				String urlForward;
				if (StringUtils.indexOfAny(userAgent, BaseMovieManagerServlet.MOBILE_USER_AGENTS) != -1) {
					urlForward = "/Auth/MovieManager?action=execInit";
					
				} else {
					urlForward = "/jsp/computer/index.jsp";
				}
				RequestDispatcher dispatch = this.getServletContext().getRequestDispatcher(urlForward);
				dispatch.forward(request, response);
		%>
	</body>
</html>