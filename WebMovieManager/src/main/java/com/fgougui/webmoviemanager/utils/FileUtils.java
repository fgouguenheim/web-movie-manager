package com.fgougui.webmoviemanager.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe utilitaire pour la manipulation des fichiers
 */
public class FileUtils {

	/**
	 * Renvoie le chemin du fichier (sans le nom de fichier)
	 * 
	 * @param urlFile
	 * String
	 * @return String
	 */
	public static String getFilePath(final String urlFile) {
		final String fileSeparator = System.getProperty("file.separator");
		final String filePath = StringUtils.substringBeforeLast(urlFile, fileSeparator) + fileSeparator;
		return filePath;
	}

	/**
	 * Renvoie le nom du dossier contenant le fichier (càd de son parent immédiat)
	 * 
	 * @param urlFile
	 * String
	 * @return String
	 */
	public static String getFileFolder(final String urlFile) {
		final String fileSeparator = System.getProperty("file.separator");
		final String filePath = StringUtils.substringBeforeLast(urlFile, fileSeparator);
		final String fileFolder = StringUtils.substringAfterLast(filePath, fileSeparator);
		return fileFolder;
	}

	/**
	 * Renvoie le nom d'un fichier avec son extension
	 * 
	 * @param urlFile
	 * String
	 * @return String
	 */
	public static String getFileNameWithExtension(final String urlFile) {
		final String fileSeparator = System.getProperty("file.separator");
		final String[] split = StringUtils.split(urlFile, fileSeparator);
		return split[split.length - 1];
	}

	/**
	 * Renvoie le nom d'un fichier sans son extension
	 * 
	 * @param urlFile
	 * String
	 * @return String
	 */
	public static String getFileNameWithoutExtension(final String urlFile) {
		final String fileSeparator = System.getProperty("file.separator");
		final String[] tmp = StringUtils.split(urlFile, fileSeparator);
		final String fileNameWithExtension = tmp[tmp.length - 1];
		final String fileName = StringUtils.substringBeforeLast(fileNameWithExtension, ".");
		return fileName;
	}

	/**
	 * Renvoie l'extension d'un fichier
	 * 
	 * @param urlFile
	 * String
	 * @return String
	 */
	public static String getFileExtension(final String urlFile) {
		final String fileSeparator = System.getProperty("file.separator");
		final String[] tmp = StringUtils.split(urlFile, fileSeparator);
		final String fileNameWithExtension = tmp[tmp.length - 1];
		final String fileExtension = StringUtils.substringAfterLast(fileNameWithExtension, ".");
		return fileExtension;
	}

	/**
	 * Renvoie "true" si le fichier dont le nom est passé en premier paramètre
	 * est contenu dans un des répertoires dont le nom est passé en second
	 * paramètre
	 * 
	 * @param urlFile
	 * @param urlFolders
	 * @return boolean
	 */
	public static boolean isInFolders(final String urlFile, final String[] urlFolders) {
		for (final String urlFolder : urlFolders) {
			if (StringUtils.startsWith(urlFile, urlFolder)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 */
	private FileUtils() {
		super();
	}

}
