package com.fgougui.webmoviemanager.utils;

import com.fgougui.webmoviemanager.utils.internal.GraphicsUtilities;
import org.apache.commons.io.IOUtils;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

public class GraphicsUtils {

	/**
	 * Creates a thumbnail of a JPEG picture (width : 170px)
	 * 
	 * @param inputPicturePath
	 * @param outputPicturePath
	 * @throws IOException
	 */
	public static void createJPGThumbnail(final String inputPicturePath, final String outputPicturePath) throws IOException {
		// Reads the source image
		final BufferedImage imageSource = ImageIO.read(new File(inputPicturePath));
		// Determinates the height from the width
		final int oldWidth = imageSource.getWidth();
		final int oldHeight = imageSource.getHeight();
		final int newWidth = 170;
		final int newHeight = (oldHeight * newWidth) / oldWidth;
		// If new dimensions are greater, just copy the picture
		if ((newWidth >= oldWidth) || (newHeight >= oldHeight)) {
			// Streams declaration and opening
			final FileInputStream sourceFile = new FileInputStream(inputPicturePath);
			final FileOutputStream destinationFile = new FileOutputStream(outputPicturePath);
			IOUtils.copy(sourceFile, destinationFile);
			IOUtils.closeQuietly(sourceFile);
			IOUtils.closeQuietly(destinationFile);
		} else {
			// Makes and writes the thumbnail
			final RenderedImage imageDest = GraphicsUtilities.createThumbnail(imageSource, newWidth, newHeight);
			ImageWriter writer = null;
			final Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
			if (iter.hasNext()) {
				writer = iter.next();
			}
			if (writer == null) {
				return;
			}
			final ImageOutputStream ios = ImageIO.createImageOutputStream(new File(outputPicturePath));
			writer.setOutput(ios);
			final ImageWriteParam iwparam = new JPEGImageWriteParam(null);
			iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			iwparam.setCompressionQuality(0.8f);
			writer.write(null, new IIOImage(imageDest, null, null), iwparam);
			ios.flush();
			writer.dispose();
			ios.close();
		}
	}

	/**
	 * 
	 */
	private GraphicsUtils() {
		super();
	}

}
