package com.fgougui.webmoviemanager.utils;

import java.text.Normalizer;

/**
 * Classe utilitaire pour les services d'encodage
 */
public class EncodingUtils {

	/**
	 * Encode un texte en HTML. Utilisé pour les getters appelés dans les jsp.
	 * 
	 * @param text
	 * String
	 * @return String encodé en HTML
	 */
	public static String textToHTML(String text) {
		if (text == null) {
			return "";
		}
		text = text.replace("\"", "&quot;").replace("\n", "<br>");
		return text;
	}

	/**
	 * Supprime les accents d'une chaine
	 * 
	 * @param text
	 * @return
	 */
	public static String removeAccents(String text) {
		return Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
	}

	/**
     * 
     */
	private EncodingUtils() {
		super();
	}

}
