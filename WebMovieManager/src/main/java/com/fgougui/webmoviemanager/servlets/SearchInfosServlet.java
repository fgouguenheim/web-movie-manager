package com.fgougui.webmoviemanager.servlets;

import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.dao.MovieManagerDao;
import com.fgougui.webmoviemanager.utils.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

/**
 * Servlet pour la recherche d'informations sur un film
 */
@SuppressWarnings("serial")
public class SearchInfosServlet extends BaseMovieManagerServlet {

	/**
	 * Lance les recherches pour les informations sur les films :
	 * soit l'url d'un fichier et un nom à chercher sont fournis en paramètre,
	 * sinon on lance la recherche sur le premier fichier dont l'url est "à renseigner"
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execSearchInfos(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {

		String nameMovieToSearch = null;
		String urlFileToSearch = null;

		if ((req.getParameter("nameMovieToSearch") == null) || (req.getParameter("urlFile") == null)) {
			// CAS 1 : on lance la recherche sur le premier fichier de la filmothèque dont l'url est "à renseigner"
			final Iterator<String> it = getMovieManager().getUrlsFilesToFill().iterator();
			if (!it.hasNext()) {
				// Plus de films à renseigner...
				forward(req, resp, "synchronisationSucces.jsp");
				return;
			} else {
				// Récupération du nom du premier fichier à renseigner
				urlFileToSearch = it.next();
				nameMovieToSearch = FileUtils.getFileNameWithoutExtension(urlFileToSearch);
			}
		} else {
			// CAS 2 : on lance la recherche sur le nom et l'url fournis en paramètres
			// Récupération du nom à rechercher et de l'url du fichier
			nameMovieToSearch = req.getParameter("nameMovieToSearch");
			urlFileToSearch = req.getParameter("urlFile");
			// On place le nom à rechercher dans la requête
			req.setAttribute("nameMovieToSearch", nameMovieToSearch);
		}
		// Appel au service
		final Set<MovieBean> movies = searchInfosServices.findMovie(nameMovieToSearch);
		// Enregistrement des données dans le contexte de la requête
		req.setAttribute("movies", movies);
		req.setAttribute("fileName", FileUtils.getFileNameWithExtension(urlFileToSearch));
		req.setAttribute("movieName", FileUtils.getFileNameWithoutExtension(urlFileToSearch));
		req.setAttribute("urlFile", urlFileToSearch);
		req.setAttribute("nbMoviesRemaining", getMovieManager().getUrlsFilesToFill().size());
		// Redirection vers la jsp
		forward(req, resp, "searchInfos.jsp");
	}

	/**
	 * Valide le film sélectionné et lance la récupération des infos
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execValidateInfos(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Récupération de l'id du film sélectionné
		final String id = req.getParameter("idMovie");
		// Récupération de l'url du film dont on va remplir les infos
		final String urlFile = req.getParameter("urlFile");
		// Récupération des flags "à voir", "déjà vu"
		final boolean toSee = Boolean.valueOf(req.getParameter("toSee"));
		final boolean alreadySeen = Boolean.valueOf(req.getParameter("alreadySeen"));
		// Récupération de la filmotheque
		final MovieManagerBean movieManager = getMovieManager();
		// Appel au service
		final MovieBean movie = searchInfosServices.fillMovie(movieManager, urlFile, id, toSee, alreadySeen);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManager);
		// Log
		MovieManagerDao.appendToMovieManagerLogs("Ajout dans la filmotheque du film " + movie.getTitle());
		// Redirection vers l'url de recherche
		forward(req, resp, "/Auth/SearchInfos?action=execSearchInfos");
	}

	/**
	 * Signale le fichier sélectionné comme n'étant pas un film
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execReportNotAMovie(final HttpServletRequest req, final HttpServletResponse resp) throws IOException,
			ServletException {
		// Récupération de l'url du film dont on va remplir les infos
		final String urlFile = req.getParameter("urlFile");
		// Récupération de la filmotheque
		final MovieManagerBean movieManager = getMovieManager();
		// Appel au service
		searchInfosServices.reportFileIsNotAMovie(movieManager, urlFile);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManager);
		// Redirection vers l'url de recherche
		forward(req, resp, "/Auth/SearchInfos?action=execSearchInfos");
	}

}
