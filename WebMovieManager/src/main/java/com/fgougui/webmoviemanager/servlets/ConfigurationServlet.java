package com.fgougui.webmoviemanager.servlets;

import com.fgougui.webmoviemanager.config.ConfigProperties;
import com.fgougui.webmoviemanager.dao.BaseDaoXStreamImpl;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.*;

/**
 * Servlet pour la gestion de la configuration
 */
@SuppressWarnings("serial")
public class ConfigurationServlet extends BaseMovieManagerServlet {

	/**
	 * Initialisation de la page "configuration"
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Get
	public final void execInit(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Placement de la configuration dans la requête
		req.setAttribute("configuration", ConfigProperties.getInstance());
		// Redirection vers la jsp
		forward(req, resp, "configuration.jsp");
	}

	/**
	 * Enregistrement de la configuration
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execSave(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Validation des paramètres
		// Les URL doivent terminer par / ou \
		final String movieManagerFilesPathCommaSeparated = transformFolderConfig(req.getParameter("movieManagerFilesPath"));
		final String[] movieManagerFilesPathTab = ConfigProperties.getInstance().splitAndTrim(movieManagerFilesPathCommaSeparated);
		final String dataFilesPath = transformFolderConfig(req.getParameter("dataFilesPath"));
		for (final String movieManagerFilesPath : movieManagerFilesPathTab) {
			if (!isFolderConfigCorrect(movieManagerFilesPath)) {
				throw new ConfigurationException(EXCEPTION_INCORRECT_FOLDER_CONFIG_MUST_END_WITH_SLASH);
			}
		}
		if (!isFolderConfigCorrect(dataFilesPath)) {
			throw new ConfigurationException(EXCEPTION_INCORRECT_FOLDER_CONFIG_MUST_END_WITH_SLASH);
		}
		// Les chemins des répertoires contenant les films ne doivent pas se contenir
		// (i.e. un chemin ne doit pas être contenu dans un autre)

		for (int i = 0; i < movieManagerFilesPathTab.length; i++) {
			for (int j = i + 1; j < movieManagerFilesPathTab.length; j++) {
				final String path1 = movieManagerFilesPathTab[i];
				final String path2 = movieManagerFilesPathTab[j];
				if (path1.startsWith(path2) || path2.startsWith(path1)) {
					throw new ConfigurationException(EXCEPTION_INCORRECT_FOLDER_CONFIG_ONE_CONTAINS_ANOTHER);
				}
			}
		}
		// Les extensions à ignorer doivent être espacées par des virgules
		final String movieManagerFilesExtensionIgnore = req.getParameter("movieManagerFilesExtensionIgnore");
		if (StringUtils.isNotBlank(movieManagerFilesExtensionIgnore) && !movieManagerFilesExtensionIgnore.matches("^([^,]+(,[^,]+)*)?$")) {
			throw new ConfigurationException(EXCEPTION_INCORRECT_EXTENSIONS_TO_IGNORE_CONFIG);
		}
		// Création du répertoire de stockage s'il n'existe pas
		final File dataFilesFolder = new File(dataFilesPath);
		if (!dataFilesFolder.exists()) {
			try {
				final boolean isFolderCreated = dataFilesFolder.mkdirs();
				if (!isFolderCreated) {
					throw new Exception();
				}
			} catch (final Exception e) {
				// Impossible de créer le répertoire
				throw new ConfigurationException("Impossible de créer le répertoire de stockage", e);
			}
		} else if (!dataFilesFolder.isDirectory()) {
			// Un fichier de même nom existe déjà
			throw new ConfigurationException("Le répertoire de stockage indiqué est incorrect : un fichier du même nom existe déjà");
		}
		// Création de ses sous-répertoires (s'il n'existent pas déjà)
		try {
			final File postersOriginalFilesPath = new File(ConfigProperties.getInstance().getPostersOriginalFilesPath());
			final File postersThumbnailFilesPath = new File(ConfigProperties.getInstance().getPostersThumbnailFilesPath());
			final String fileSystemSeparator = ConfigProperties.getInstance().getFileSystemSeparator();
			final File backupFilesPath = new File(ConfigProperties.getInstance().getDataFilesPath() + BaseDaoXStreamImpl.BACKUP_FOLDER_NAME
					+ fileSystemSeparator);
			final boolean isFolderCreated = ((postersOriginalFilesPath.exists() && postersOriginalFilesPath.isDirectory()) || postersOriginalFilesPath
					.mkdir())
					&& ((postersThumbnailFilesPath.exists() && postersThumbnailFilesPath.isDirectory()) || postersThumbnailFilesPath
							.mkdir()) && ((backupFilesPath.exists() && backupFilesPath.isDirectory()) || backupFilesPath.mkdir());
			if (!isFolderCreated) {
				throw new Exception();
			}
		} catch (final Exception e) {
			// Impossible de créer le répertoire
			throw new ConfigurationException("Impossible de créer le répertoire de stockage", e);
		}

		// Enregistrement des paramètres
		ConfigProperties.getInstance().setMovieManagerFilesPathCommaSeparated(movieManagerFilesPathCommaSeparated);
		ConfigProperties.getInstance().setMovieManagerFilesExtensionIgnoreCommaSeparated(movieManagerFilesExtensionIgnore);
		ConfigProperties.getInstance().setMovieManagerAutoRename(
				StringUtils.isNotBlank(req.getParameter("movieManagerAutoRename")) ? true : false);
		ConfigProperties.getInstance().setDataFilesPath(dataFilesPath);
		ConfigProperties.getInstance().setDataBackup(StringUtils.isNotBlank(req.getParameter("dataBackup")) ? true : false);
		ConfigProperties.getInstance().setNetworkProxy(StringUtils.isNotBlank(req.getParameter("networkProxy")) ? true : false);
		ConfigProperties.getInstance().setNetworkProxyHost(req.getParameter("networkProxyHost"));
		ConfigProperties.getInstance().setNetworkProxyPort(req.getParameter("networkProxyPort"));
		ConfigProperties.getInstance().setNetworkProxyLogin(req.getParameter("networkProxyLogin"));
		ConfigProperties.getInstance().setNetworkProxyPassword(req.getParameter("networkProxyPassword"));
		ConfigProperties.getInstance().writeProperties();
		// Redirection vers l'url de la filmothèque
		forward(req, resp, "configurationSucces.jsp");
	}

	/**
	 * Transforme les \ en / pour les config de chemins
	 * 
	 * @param folderPathName
	 * @return String
	 */
	private String transformFolderConfig(final String folderPathName) {
		return StringUtils.replaceChars(folderPathName, '\\', '/');
		// if (folderPathName == null) {
		// return null;
		// }
		// return folderPathName.replaceAll("\\", "/");
	}

	/**
	 * Indique si un chemin de répertoire est configuré correctement, càd s'il
	 * finit par / ou qu'il est vide
	 * 
	 * @param folderPathName
	 * String
	 * @return boolean
	 */
	private boolean isFolderConfigCorrect(final String folderPathName) {
		return (StringUtils.isNotBlank(folderPathName) && !folderPathName.endsWith("/")) ? false : true;
	}

}
