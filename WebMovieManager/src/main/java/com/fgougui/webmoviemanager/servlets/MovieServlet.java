package com.fgougui.webmoviemanager.servlets;

import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.dao.MovieManagerDao;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.exceptions.FunctionalException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;

import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.EXCEPTION_INCORRECT_PARAMETERS;
import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.EXCEPTION_JSON;

/**
 * Servlet pour la gestion d'un film
 */
@SuppressWarnings("serial")
public class MovieServlet extends BaseMovieManagerServlet {

	private MovieBean getMovieFromId(final HttpServletRequest req) throws FunctionalException, DataException, ConfigurationException {
		// Récupération de l'id du film sélectionné
		Long idMovie = null;
		try {
			idMovie = Long.parseLong(req.getParameter("idMovie"));
		} catch (final NumberFormatException e) {
			throw new FunctionalException(EXCEPTION_INCORRECT_PARAMETERS, e);
		}
		// Récupération de la filmotheque
		final MovieManagerBean movieManager = getMovieManager();
		// Récupération du film
		final MovieBean movie = movieManager.getMovie(idMovie);
		if (movie == null) {
			throw new FunctionalException(EXCEPTION_INCORRECT_PARAMETERS);
		}
		return movie;
	}

	/**
	 * Affiche les informations sur le film
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Get
	public final void execDisplayInfos(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		// Enregistrement du film en requête
		req.setAttribute("movie", movie);
		// Redirection vers la jsp
		forward(req, resp, "movieInfos.jsp");
	}

	/**
	 * Affiche la jaquette film (taille originale)
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Get
	public final void execDisplayPosterOriginal(final HttpServletRequest req, final HttpServletResponse resp) throws IOException,
			ServletException {
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		// Récupération et envoi de la jaquette
		final String urlPoster = movie.getUrlPosterOriginal();
		// Envoi du poster
		execDisplayPoster(urlPoster, resp);
	}

	/**
	 * Affiche la jaquette film (taille miniature)
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Get
	public final void execDisplayPosterThumbnail(final HttpServletRequest req, final HttpServletResponse resp) throws IOException,
			ServletException {
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		// Récupération et envoi de la jaquette
		final String urlPoster = movie.getUrlPosterThumbnail();
		// Envoi du poster
		execDisplayPoster(urlPoster, resp);
	}

	private void execDisplayPoster(final String urlPoster, final HttpServletResponse resp) throws IOException {
		// Envoi du poster
		final File f = new File(urlPoster);
		final ServletOutputStream op = resp.getOutputStream();
		final ServletContext context = getServletConfig().getServletContext();
		final String mimetype = context.getMimeType(urlPoster);
		resp.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
		resp.setContentLength((int) f.length());
		final DataInputStream in = new DataInputStream(new FileInputStream(f));
		IOUtils.copy(in, op);
		IOUtils.closeQuietly(in);
		op.flush();
		IOUtils.closeQuietly(op);

	}

	/**
	 * Met à jour les informations sur le film à partir d'allociné
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Get
	public final void execUpdateInfos(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Récupération de la filmotheque
		final MovieManagerBean movieManagerBean = getMovieManager();
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		// Le film avait-il un poster avant ?
		final boolean hasPosterBefore = movie.isPoster();
		// Mise à jour
		searchInfosServices.updateMovie(movie);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManagerBean);
		// Remplissage du flux JSON
		final JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("pressRating", new DecimalFormat("#.#").format(movie.getPressRating()).replace(',', '.'));
			jsonObject.put("userRating", new DecimalFormat("#.#").format(movie.getUserRating()).replace(',', '.'));
			// Le film a-t-il un poster qu'il n'avait pas avant ?
			final boolean hasNewPoster = !hasPosterBefore && movie.isPoster();
			jsonObject.put("hasNewPoster", hasNewPoster);
		} catch (final JSONException e) {
			throw new FunctionalException(EXCEPTION_JSON, e);
		}
		// Renvoi du flux JSON
		forwardJSONObject(req, resp, jsonObject);
	}

	/**
	 * Marque un film comme "à voir" (et vice versa)
	 * 
	 * @param req
	 * @param resp
	 * @throws ConfigurationException
	 * @throws DataException
	 */
	@Post
	public final void execSetToSee(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException {
		// Récupération de la filmotheque
		final MovieManagerBean movieManagerBean = getMovieManager();
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		boolean toSee;
		try {
			// Récupération du marqueur 'à voir'
			toSee = Boolean.parseBoolean(req.getParameter("toSee"));
		} catch (final Exception e) {
			// Erreur de parsing
			throw new FunctionalException(EXCEPTION_INCORRECT_PARAMETERS, e);
		}
		// Mise à jour du film
		movie.setToSee(toSee);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManagerBean);
		// // Succès
		// forwardSuccess(req, resp);
	}

	/**
	 * Marque un film comme "vu" (et vice versa)
	 * 
	 * @param req
	 * @param resp
	 * @throws ConfigurationException
	 * @throws DataException
	 */
	@Post
	public final void execSetAlreadySeen(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException {
		// Récupération de la filmotheque
		final MovieManagerBean movieManagerBean = getMovieManager();
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		boolean alreadySeen;
		try {
			// Récupération du marqueur 'à voir'
			alreadySeen = Boolean.parseBoolean(req.getParameter("alreadySeen"));
		} catch (final Exception e) {
			// Erreur de parsing
			throw new FunctionalException(EXCEPTION_INCORRECT_PARAMETERS, e);
		}
		// Mise à jour du film
		movie.setAlreadySeen(alreadySeen);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManagerBean);
		// // Succès
		// forwardSuccess(req, resp);
	}

	/**
	 * Renvoie le film au client pour watchMovie
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	@Get
	public final void execWatchMovie(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
		// Récupération du chemin du film à visualiser
		final String urlFile = req.getParameter("urlFile");
		// Récupération du nom du fichier
		final String fileSeparator = System.getProperty("file.separator");
		final String[] tmp = StringUtils.split(urlFile, fileSeparator);
		final String fileNameWithExtension = tmp[tmp.length - 1];
		// Récupération et envoi du fichier
		final File f = new File(urlFile);
		final ServletOutputStream op = resp.getOutputStream();
		final ServletContext context = getServletConfig().getServletContext();
		final String mimetype = context.getMimeType(urlFile);
		resp.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
		resp.setContentLength((int) f.length());
		resp.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameWithExtension + "\"");
		resp.setContentLength((int) f.length());
		final DataInputStream in = new DataInputStream(new FileInputStream(f));
		IOUtils.copy(in, op);
		IOUtils.closeQuietly(in);
		op.flush();
		IOUtils.closeQuietly(op);
	}

	/**
	 * Supprime le film de la filmotheque
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execDeleteFromMovieManager(final HttpServletRequest req, final HttpServletResponse resp) throws IOException,
			ServletException {
		// Récupération de la filmotheque
		final MovieManagerBean movieManagerBean = getMovieManager();
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		// Suppression du film de la filmotheque
		movieManagerBean.getMovies().remove(movie);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManagerBean);
		// Log
		MovieManagerDao.appendToMovieManagerLogs("Suppression de la filmotheque du film " + movie.getTitle());
		// Redirection vers la jsp
		forward(req, resp, "movieManager.jsp");
	}

	/**
	 * Supprime le film du disque dur
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execDeleteFromDisk(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Récupération de la filmotheque
		final MovieManagerBean movieManagerBean = getMovieManager();
		// Récupération du film
		final MovieBean movie = getMovieFromId(req);
		// Suppression du film du disque
		final String urlFile = movie.getUrlFile();
		fileServices.deleteFile(urlFile);
		// Suppression du film de la filmotheque
		movieManagerBean.getMovies().remove(movie);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManagerBean);
		// Log
		MovieManagerDao.appendToMovieManagerLogs("Suppression du disque du fichier " + movie.getUrlFile());
		// Redirection vers la jsp
		forward(req, resp, "movieManager.jsp");
	}

}
