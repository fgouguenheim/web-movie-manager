package com.fgougui.webmoviemanager.servlets;

import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.config.ConfigProperties;
import com.fgougui.webmoviemanager.dao.MovieManagerDao;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * Servlet pour la gestion de la filmotheque
 */
@SuppressWarnings("serial")
public class MovieManagerServlet extends BaseMovieManagerServlet {

	/**
	 * Initialisation de la page "movieManager"
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Get
	public final void execInit(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Initialisation de la filmothèque
		getMovieManager();
		// Redirection vers la jsp
		forward(req, resp, "movieManager.jsp");
	}

	/**
	 * Initialisation de la liste des films pour tri (mobile)
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Get
	public final void execFilter(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Redirection vers la jsp
		forward(req, resp, "listMovies.jsp");
	}

	/**
	 * Met à jour les informations sur tous les films à partir d'allociné
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execUpdateAllInfos(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Récupération de la filmotheque
		final MovieManagerBean movieManagerBean = getMovieManager();
		// Pour chaque film
		for (final MovieBean movie : movieManagerBean.getMovies()) {
			// Si les 2 critiques ne sont pas renseignées
			if ((movie.getPressRating() == 0) || (movie.getUserRating() == 0)) {
				// Mise à jour
				searchInfosServices.updateMovie(movie);
			}
		}
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManagerBean);
	}

	/**
	 * Recherche de films
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execSearchMovies(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Récupération de la chaine de recherche
		final String pattern = req.getParameter("q");
		// Recherche
		final Collection<MovieBean> movies = getMovieManager().getMoviesByPattern(pattern);
		// Enregistrement du résultat en requête
		req.setAttribute("movies", movies);
		// Redirection vers la jsp
		forward(req, resp, "searchMovies.jsp");
	}

	/**
	 * Lancement de la synchronisation
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	@Post
	public final void execSynchro(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, ServletException {
		// Récupération de la filmotheque
		final MovieManagerBean movieManagerBean = getMovieManager();
		// Récupération des chemins vers les répertoires contenant les films
		final String[] movieManagerFilesPath = ConfigProperties.getInstance().getMovieManagerFilesPath();
		// Récupération des extensions à ignorer (s'il y en a)
		String[] movieManagerFilesExtensionIgnore;
		try {
			movieManagerFilesExtensionIgnore = ConfigProperties.getInstance().getMovieManagerFilesExtensionIgnore();
		} catch (final ConfigurationException e) {
			movieManagerFilesExtensionIgnore = new String[0];
		}
		// Appel au service
		synchronisationServices.synchronise(movieManagerBean, movieManagerFilesPath, movieManagerFilesExtensionIgnore);
		// Enregistrement des données
		MovieManagerDao.updateMovieManagerBean(movieManagerBean);
		// Redirection vers l'url de recherche
		forward(req, resp, "/Auth/SearchInfos?action=execSearchInfos");
	}

}
