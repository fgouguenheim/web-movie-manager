package com.fgougui.webmoviemanager.servlets;

import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.dao.MovieManagerDao;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.services.FileServices;
import com.fgougui.webmoviemanager.services.SearchInfosServices;
import com.fgougui.webmoviemanager.services.SynchronisationServices;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet mère pour l'application
 */
@SuppressWarnings("serial")
@Component
public abstract class BaseMovieManagerServlet extends BaseServlet {

	/** Service de gestion de fichiers */
	protected FileServices fileServices;

	/** Service de synchronisation */
	protected SynchronisationServices synchronisationServices;

	/** Service de récupération des infos */
	protected SearchInfosServices searchInfosServices;

	/**
	 * Initialisation de la servlet
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		// Initialisation des services
		final WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		fileServices = (FileServices) wac.getBean("fileServices");
		synchronisationServices = (SynchronisationServices) wac.getBean("synchronisationServices");
		searchInfosServices = (SearchInfosServices) wac.getBean("searchInfosServices");
	}

	/** Agents utilisateurs pour la version mobile */
	public static final String[] MOBILE_USER_AGENTS = { "android", "iphone" };

	/**
	 * Renvoie la filmothèque depuis le contexte de l'application
	 * 
	 * @return MovieManagerBean
	 * @throws ConfigurationException
	 * @throws DataException
	 */
	public MovieManagerBean getMovieManager() throws DataException, ConfigurationException {
		// On vérifie que la filmothèque a été initialisée
		MovieManagerBean movieManager = (MovieManagerBean) getServletContext().getAttribute("movieManager");
		if (movieManager == null) {
			// Initialisation de la filmothèque :
			// Récupération des données depuis le disque
			movieManager = MovieManagerDao.getMovieManagerBean();
			// Enregistrement des données dans le contexte de l'application
			getServletContext().setAttribute("movieManager", movieManager);
		}
		return movieManager;
	}

	/**
	 * Redirige la réponse vers une ressource (sans redirection du navigateur)
	 * 
	 * @param req
	 * @param resp
	 * @param ressourcePath
	 * @throws ServletException
	 * @throws IOException
	 */
	protected final void forward(final HttpServletRequest req, final HttpServletResponse resp, String ressourcePath)
			throws ServletException, IOException {
		// Dans le cas des jsp
		if (ressourcePath.endsWith(".jsp")) {
			// Selon le type de client (ordinateur/mobile), on va chercher la jsp qui convient
			String typeClient;
			final String userAgent = req.getHeader("User-Agent").toLowerCase();
			if (StringUtils.indexOfAny(userAgent, MOBILE_USER_AGENTS) != -1) {
				typeClient = "mobile";
			} else {
				typeClient = "computer";
			}
			ressourcePath = "/jsp/" + typeClient + "/" + ressourcePath;
		}
		// Redirection vers la ressource
		final RequestDispatcher dispatch = getServletContext().getRequestDispatcher(ressourcePath);
		dispatch.forward(req, resp);
	}

	/**
	 * Redirige la réponse vers une ressource (redirection du navigateur)
	 * 
	 * @param resp
	 * @param ressourcePath
	 * @throws IOException
	 */
	protected final void redirect(final HttpServletResponse resp, final String ressourcePath) throws IOException {
		final String urlWithSessionID = resp.encodeRedirectURL(ressourcePath);
		resp.sendRedirect(urlWithSessionID);
	}

}
