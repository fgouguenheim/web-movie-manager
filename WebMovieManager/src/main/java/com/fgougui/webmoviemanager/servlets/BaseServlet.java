package com.fgougui.webmoviemanager.servlets;

import org.json.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Servlet mère pour toutes les servlets de l'application
 */
@SuppressWarnings("serial")
public abstract class BaseServlet extends HttpServlet {

	/** Annotation pour les méthodes de type GET */
	@Documented
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Get {
	}

	/** Annotation pour les méthodes de type POST */
	@Documented
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Post {
	}

	/** Map contenant les méthodes de type Get */
	protected HashMap<String, Method> actionsGet;

	/** Map contenant les méthodes de type Post */
	protected HashMap<String, Method> actionsPost;

	/**
	 * Initialisation de la servlet
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		// Initialisation des hashmaps d'actions
		actionsGet = new HashMap<String, Method>();
		actionsPost = new HashMap<String, Method>();
		// Lecture des methodes de la classe
		final Method[] methods = this.getClass().getMethods();
		for (final Method method : methods) {
			if (method.isAnnotationPresent(Get.class)) {
				actionsGet.put(method.getName(), method);
			} else if (method.isAnnotationPresent(Post.class)) {
				actionsPost.put(method.getName(), method);
			}
		}
	}

	/**
	 * Traite les requêtes de type GET
	 * 
	 * @param req
	 * HttpServletRequest
	 * @param resp
	 * HttpServletResponse
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		invokeAction(actionsGet, req, resp);
	}

	/**
	 * Traite les requêtes de type POST
	 * 
	 * @param req
	 * HttpServletRequest
	 * @param resp
	 * HttpServletResponse
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		invokeAction(actionsPost, req, resp);
	}

	/**
	 * Invoque la méthode de la servlet correspondant au paramètre "action" de la requête
	 * 
	 * @param actions
	 * Map d'actions
	 * @param req
	 * HttpServletRequest
	 * @param resp
	 * HttpServletResponse
	 * @throws IOException
	 * @throws ServletException
	 */
	private void invokeAction(final HashMap<String, Method> actions, final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			// Récupération de l'action demandee
			final String actionString = req.getParameter("action");
			final Method action = actions.get(actionString);
			// Execution de la methode
			action.invoke(this, req, resp);
		} catch (final InvocationTargetException e) {
			e.getTargetException().printStackTrace();
			String message = e.getCause().getMessage();
			if (message == null) {
				message = "Erreur interne.";
			}
			forwardException(req, resp, e, message);
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
			forwardException(req, resp, e, "Erreur interne.");
		}
	}

	/**
	 * Redirige la réponse vers la ressource indiquant l'exception
	 * 
	 * @param req
	 * HttpServletRequest
	 * @param resp
	 * HttpServletResponse
	 * @param exception
	 * Exception
	 * @param message
	 * String
	 * @throws ServletException
	 * @throws IOException
	 */
	protected final void forwardException(final HttpServletRequest req, final HttpServletResponse resp, final Exception exception,
			final String message) throws ServletException, IOException {
		// On place le message dans la requête
		req.setAttribute("message", message);
		// Redirection vers la jsp
		final RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/jsp/xml/exception.jsp");
		dispatch.forward(req, resp);
	}

	/**
	 * Redirige la réponse vers la ressource décrivant l'objet JSON
	 * 
	 * @param req
	 * HttpServletRequest
	 * @param resp
	 * HttpServletResponse
	 * @param jsonObject
	 * JSONObject
	 * @throws ServletException
	 * @throws IOException
	 */
	protected final void forwardJSONObject(final HttpServletRequest req, final HttpServletResponse resp, final JSONObject jsonObject)
			throws ServletException, IOException {
		// On place le message dans la requête
		req.setAttribute("jsonObject", jsonObject);
		// Redirection vers la jsp
		final RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/jsp/json/jsonObject.jsp");
		dispatch.forward(req, resp);
	}
}
