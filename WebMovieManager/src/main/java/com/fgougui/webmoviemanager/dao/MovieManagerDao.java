package com.fgougui.webmoviemanager.dao;

import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;

public final class MovieManagerDao {

	/** Implémentation du DAO */
	private static final BaseDao BASE_DAO = DaoFactory.getBaseDao();

	private MovieManagerDao() {
		super();
	}

	/**
	 * Lit la filmotheque sur le disque
	 * 
	 * @return MovieManagerBean
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	public static MovieManagerBean getMovieManagerBean() throws DataException, ConfigurationException {
		final MovieManagerBean result = (MovieManagerBean) BASE_DAO.getBean("movieManager");
		return (result == null) ? new MovieManagerBean() : result;
	}

	/**
	 * Met à jour la filmotheque sur le disque
	 * 
	 * @param movieManagerBean
	 * MovieManagerBean
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	public static void updateMovieManagerBean(final MovieManagerBean movieManagerBean) throws DataException, ConfigurationException {
		BASE_DAO.updateBean(movieManagerBean, "movieManager");
	}

	/**
	 * Lit la filmotheque sur le disque
	 * 
	 * @return MovieManagerBean
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	public static String getMovieManagerLogs() throws DataException, ConfigurationException {
		return BASE_DAO.getLog("movieManager");
	}

	/**
	 * Met à jour le log de la filmotheque sur le disque
	 * 
	 * @param filmotheque MovieManagerBean
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	public static void appendToMovieManagerLogs(final String log) throws DataException, ConfigurationException {
		BASE_DAO.appendToLog(log, "movieManager");
	}

}
