/**
 * 
 */
package com.fgougui.webmoviemanager.dao;

import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;

import java.io.Serializable;

/**
 * Interface d'accès aux données
 */
public interface BaseDao {

	/**
	 * Ouvre un objet depuis le disque
	 * 
	 * @param fileName
	 * nom de fichier à ouvrir
	 * @return DefaultBean
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	Serializable getBean(final String fileName) throws DataException, ConfigurationException;

	/**
	 * Met a jour l'objet sur le disque
	 * 
	 * @param object
	 * DefaultBean
	 * @param fileName
	 * nom de fichier à ouvrir
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	void updateBean(final Serializable object, final String fileName) throws DataException, ConfigurationException;

	/**
	 * Enregistre une sauvegarde de l'objet sur le disque
	 * 
	 * @param object
	 * DefaultBean
	 * @param fileName
	 * nom de fichier à ouvrir
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	void saveBeanBackUp(final Serializable object, final String fileName) throws DataException, ConfigurationException;

	/**
	 * Ouvre un fichier texte depuis le disque
	 * 
	 * @param fileName
	 * nom de fichier à ouvrir
	 * @return String
	 */
	String getLog(final String fileName) throws DataException, ConfigurationException;

	/**
	 * Ajoute du texte dans un fichier texte sur le disque
	 * 
	 * @param string Chaine à ajouter
	 * @param fileName nom de fichier à ouvrir
	 * @throws ConfigurationException
	 * @throws DataException
	 */
	void appendToLog(final String string, final String fileName) throws DataException, ConfigurationException;

}
