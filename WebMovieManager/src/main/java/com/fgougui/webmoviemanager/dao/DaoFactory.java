package com.fgougui.webmoviemanager.dao;

public class DaoFactory {

    private DaoFactory() {
        super();
    }

    public static BaseDao getBaseDao() {
        return BaseDaoXStreamImpl.getInstance();
    }
}
