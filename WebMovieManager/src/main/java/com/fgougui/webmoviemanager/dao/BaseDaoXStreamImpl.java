package com.fgougui.webmoviemanager.dao;

import com.fgougui.webmoviemanager.config.ConfigProperties;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.*;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.*;

/**
 * Implémentation d'accès aux données utilisant XStream pour stocker les données
 */
public class BaseDaoXStreamImpl implements BaseDao {

	public static final String BACKUP_FOLDER_NAME = "backup";

	private static BaseDao instance = null;

	/**
	 * Renvoie le singleton
	 * @return BaseDao
	 */
	public static synchronized BaseDao getInstance() {
		if (instance == null) {
			instance = new BaseDaoXStreamImpl();
		}
		return instance;
	}

	/**
	 * Renvoie l'URL d'un fichier de log
	 * 
	 * @param fileName nom de fichier
	 * @return String
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	private String getURLLogFile(final String fileName) throws DataException, ConfigurationException {
		return ConfigProperties.getInstance().getDataFilesPath() + fileName + ".log";
	}

	/**
	 * Renvoie l'URL d'un fichier de stockage xml
	 * 
	 * @param fileName nom de fichier
	 * @return String
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	private String getURLDataFile(final String fileName) throws DataException, ConfigurationException {
		return ConfigProperties.getInstance().getDataFilesPath() + fileName + ".xml";
	}

	/**
	 * Renvoie l'URL d'un fichier de sauvegarde en fonction de la date
	 * 
	 * @param date date
	 * @param fileName nom de fichier
	 * @return String
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	private String getURLDataBackupFile(final DateTime date, final String fileName) throws DataException, ConfigurationException {
		final String fileNameBackup = fileName + '-' + date.getHourOfDay() + "h" + date.getMinuteOfHour() + "m" + date.getSecondOfMinute()
				+ "s" + date.getMillisOfSecond() + "ms";
		final String urlObjectFile = getPathDataBackupFile(date) + fileNameBackup + ".xml";
		return urlObjectFile;
	}

	/**
	 * Renvoie le chemin de sauvegarde en fonction de la date
	 * 
	 * @param date date
	 * @return String
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	private String getPathDataBackupFile(final DateTime date) throws DataException, ConfigurationException {
		final String dateString = DateTimeFormat.forPattern("yyyy-MM-dd").print(date);
		final String fileSystemSeparator = ConfigProperties.getInstance().getFileSystemSeparator();
		final String pathBackup = ConfigProperties.getInstance().getDataFilesPath() + BACKUP_FOLDER_NAME + fileSystemSeparator + dateString
				+ fileSystemSeparator;
		return pathBackup;
	}

	@Override
	public final Serializable getBean(final String fileName) throws DataException, ConfigurationException {
		final String urlObjectFile = getURLDataFile(fileName);
		Serializable objectToReturn;
		try {
			// Ouverture du fichier
			final FileInputStream fileInputStream = new FileInputStream(urlObjectFile);
			try {
				// Lecture de l'objet
				final XStream xstream = new XStream(new DomDriver("UTF-8"));
				objectToReturn = (Serializable) xstream.fromXML(fileInputStream);
			} catch (final Exception e) {
				// Le fichier existe mais est illisible
				throw new DataException(EXCEPTION_FILE_READ, e);
			} finally {
				// Fermeture du décodeur
				IOUtils.closeQuietly(fileInputStream);
			}
			// Renvoi de l'objet lu
			return objectToReturn;

		} catch (final FileNotFoundException e) {
			// On ne renvoie rien
			return null;
		}
	}

	@Override
	public final void updateBean(final Serializable object, final String fileName) throws DataException, ConfigurationException {
		final String urlObjectFile = getURLDataFile(fileName);
		// Ouverture du fichier
		FileOutputStream fileOutputStream;
		try {
			fileOutputStream = new FileOutputStream(urlObjectFile);
		} catch (final FileNotFoundException e) {
			// Impossible d'écrire le fichier
			throw new DataException(EXCEPTION_FILE_WRITE, e);
		}
		// Ecriture dans le fichier
		final XStream xstream = new XStream(new DomDriver("UTF-8"));
		xstream.toXML(object, fileOutputStream);
		// Fermeture du fichier
		IOUtils.closeQuietly(fileOutputStream);
		// Sauvegarde éventuelle
		if (ConfigProperties.getInstance().isDataBackup()) {
			saveBeanBackUp(object, fileName);
		}
	}

	@Override
	public final void saveBeanBackUp(final Serializable object, final String fileName) throws DataException, ConfigurationException {
		final DateTime date = DateTime.now();
		// Si le répertoire de backup n'existe pas, on le crée
		final File file = new File(getPathDataBackupFile(date));
		file.mkdirs();
		// On récupère l'adresse complète du fichier
		final String urlObjectFile = getURLDataBackupFile(date, fileName);
		// Ouverture du fichier
		FileOutputStream fileOutputStream;
		try {
			fileOutputStream = new FileOutputStream(urlObjectFile);
		} catch (final FileNotFoundException e) {
			// Impossible d'écrire le fichier
			throw new DataException(EXCEPTION_FILE_WRITE, e);
		}
		// Ecriture dans le fichier
		final XStream xstream = new XStream(new DomDriver("UTF-8"));
		xstream.toXML(object, fileOutputStream);
		// Fermeture du fichier
		IOUtils.closeQuietly(fileOutputStream);
	}

	@Override
	public final String getLog(final String fileName) throws DataException, ConfigurationException {
		final String urlFile = getURLLogFile(fileName);
		String chaine = null;
		// Ouverture et lecture du fichier
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(urlFile);
		} catch (final FileNotFoundException e) {
			// Le fichier n'existe pas
			throw new DataException(EXCEPTION_FILE_NOT_EXISTS, e);
		}
		try {
			chaine = IOUtils.toString(fileInputStream);
		} catch (final IOException e) {
			throw new DataException(EXCEPTION_FILE_READ, e);
		}
		// Fermeture du fichier
		IOUtils.closeQuietly(fileInputStream);
		return chaine;
	}

	@Override
	public final void appendToLog(final String string, final String fileName) throws DataException, ConfigurationException {
		// Format de log
		final Date dateDepart = new Date();
		// SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM hh:mm" );
		final DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.FRANCE);
		final String date = dateFormat.format(dateDepart);
		final String log = date + " " + string + "\n";
		// Ecriture
		final String urlFile = getURLLogFile(fileName);
		try {
			final OutputStreamWriter out = new FileWriter(urlFile, true);
			IOUtils.write(log, out);
			IOUtils.closeQuietly(out);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
