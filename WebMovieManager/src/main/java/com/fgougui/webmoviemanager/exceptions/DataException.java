package com.fgougui.webmoviemanager.exceptions;

import javax.servlet.ServletException;

/**
 * Exception liée aux données et à la persistance
 */
@SuppressWarnings("serial")
public class DataException extends ServletException {

	/** Description générale de l'exception */
	private static final String GLOBAL_MSG = "Erreur back office : ";

	/**
	 * @param message Message
	 */
	public DataException(final String message) {
		super(message);
	}

	/**
	 * @param message Message
	 * @param rootCause Cause
	 */
	public DataException(final String message, final Throwable rootCause) {
		super(message, rootCause);
	}

	@Override
	public String getMessage() {
		return GLOBAL_MSG + super.getMessage();
	}
}
