package com.fgougui.webmoviemanager.exceptions;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;

/**
 * Exception de configuration
 */
@SuppressWarnings("serial")
public class ConfigurationException extends ServletException {

	/** Description générale de l'exception */
	private static final String GLOBAL_MSG = "Veuillez configurer l'application.";

	/**
	 * 
	 */
	public ConfigurationException() {
		super();
	}

	/**
	 * @param message Message
	 */
	public ConfigurationException(final String message) {
		super(message);
	}

	/**
	 * @param message Message
	 * @param rootCause Cause
	 */
	public ConfigurationException(final String message, final Throwable rootCause) {
		super(message, rootCause);
	}

	@Override
	public String getMessage() {
		if (StringUtils.isNotBlank(super.getMessage())) {
			return super.getMessage();
		} else {
			return GLOBAL_MSG;
		}
	}

}
