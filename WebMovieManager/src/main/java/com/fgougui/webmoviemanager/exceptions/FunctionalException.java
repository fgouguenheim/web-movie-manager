package com.fgougui.webmoviemanager.exceptions;

import javax.servlet.ServletException;

/**
 * Exception métier
 */
@SuppressWarnings("serial")
public class FunctionalException extends ServletException {

	/** Description générale de l'exception */
	private static final String GLOBAL_MSG = "Opération impossible : ";

	/**
	 * @param message Message
	 */
	public FunctionalException(final String message) {
		super(message);
	}

	/**
	 * @param message Message
	 * @param rootCause Cause
	 */
	public FunctionalException(final String message, final Throwable rootCause) {
		super(message, rootCause);
	}

	@Override
	public String getMessage() {
		return GLOBAL_MSG + super.getMessage();
	}
}
