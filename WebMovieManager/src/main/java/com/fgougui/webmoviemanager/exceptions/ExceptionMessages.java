/**
 * 
 */
package com.fgougui.webmoviemanager.exceptions;

/**
 * @author c82fgou
 * 
 */
public class ExceptionMessages {

	// ERREURS GENERALES

	/** Erreur de construction de l'objet JSON */
	public static final String EXCEPTION_JSON = "Erreur lors de la construction de l'objet JSON.";

	/** Paramètes incorrects */
	public static final String EXCEPTION_INCORRECT_PARAMETERS = "Paramètres incorrects...";

	/** Erreur de connexion reseau */
	public static final String EXCEPTION_CONNEXION = "Erreur de connexion réseau.";

	// DATE

	/** Exception date incorrecte */
	public static final String EXCEPTION_DATE_CORRECT = "Date incorrecte";

	/** Exception date non comprise dans les bornes */
	public static final String EXCEPTION_DATE_MIN_MAX = "La date doit être comprise entre l'an 1900 et 2100";

	// CONFIGURATION

	/** Config de dossiers incorrecte */
	public static final String EXCEPTION_INCORRECT_FOLDER_CONFIG_MUST_END_WITH_SLASH = "Les chemins des dossiers saisis doivent terminer par / ou \\";

	/** Config de dossiers incorrecte */
	public static final String EXCEPTION_INCORRECT_FOLDER_CONFIG_ONE_CONTAINS_ANOTHER = "Les chemins des dossiers ne doivent pas se contenir mutuellement";

	/** Config des extensions à ignorer incorrecte */
	public static final String EXCEPTION_INCORRECT_EXTENSIONS_TO_IGNORE_CONFIG = "Les extensions à ignorer sont mal configurées";

	// ACCES AUX DONNEES

	/** Exception lors de la lecture de l'objet dans le fichier */
	public static final String EXCEPTION_FILE_READ = "Fichier illisible ou corrompu.";

	/** Exception lors de l'écriture dans le fichier */
	public static final String EXCEPTION_FILE_WRITE = "Impossible d'écrire le fichier.";

	/** Le fichier n'existe pas */
	public static final String EXCEPTION_FILE_NOT_EXISTS = "Le fichier n'existe pas";

	// RECHERCHE DES INFOS

	/** Exception lors de l'écriture dans le fichier */
	public static final String EXCEPTION_FILE_POSTER = "Impossible d'enregistrer la jaquette.";

	/** Erreur de parsage des résultats */
	public static final String EXCEPTION_PARSE_JSON = "Impossible de recuperer les informations depuis AlloCine... (Erreur parsage JSON)";

	/** Erreur de doublon */
	public static final String EXCEPTION_MOVIE_DOUBLE = "Ce film est déjà présent dans la filmothèque a un autre endroit.<br/>";

	public static final String EXCEPTION_MOVIE_DOUBLE_URL_PRESENT = "Url du fichier déjà présent :";

	public static final String EXCEPTION_MOVIE_DOUBLE_URL_NEW = ".<br/>Url du nouveau fichier :";

	/** Erreur dans l'id */
	public static final String EXCEPTION_ID_NOT_INTEGER = "L'identifiant doit etre un nombre entier";

	/** Erreur dans l'API TheMovieDB */
	public static final String EXCEPTION_THEMOVIEDB = "Erreur lors de la connexion à TheMovieDB";

}
