package com.fgougui.webmoviemanager.beans;

import com.fgougui.webmoviemanager.config.ConfigProperties;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Film
 */
@SuppressWarnings("serial")
public class MovieBean implements Serializable, Comparable<MovieBean> {

	public enum Language {
		VO, VOST, VF
	}

	public enum Provider {
		ALLOCINE, THEMOVIEDB
	}

	/**
	 * Fournisseur de données
	 */
	private Provider provider;

	/**
	 * Identifiant du film
	 */
	private Long id;

	/**
	 * Url du fichier
	 */
	private String urlFile;

	/**
	 * A voir ?
	 */
	private boolean toSee;

	/**
	 * Déjà vu ?
	 */
	private boolean alreadySeen;

	/**
	 * VO/VOST/VF
	 */
	private Language language;

	/**
	 * Indique si la jaquette est presente (si oui, elle est récupérée sur le
	 * disque à partir de l'id du film)
	 */
	private boolean poster;

	/**
	 * URL du poster (online)
	 */
	private String urlPoster;

	/**
	 * Bande annonce
	 */
	private String urlTrailer;

	/**
	 * Titre du film
	 */
	private String title;

	/**
	 * Titre original du film
	 */
	private String titleOriginal;

	/**
	 * Année de production
	 */
	private String year;

	/**
	 * Informations complémentaires
	 */
	private String otherInfos;

	/**
	 * Resumé
	 *
	 * @deprecated n'est plus utilisé
	 */
	@Deprecated
	private String resume;

	/**
	 * Synopsis du film
	 */
	private String synopsis;

	/**
	 * Genres
	 */
	private List<String> genres = new ArrayList<String>();

	/**
	 * Nationalites
	 */
	private List<String> nationalities = new ArrayList<String>();

	/**
	 * Réalisateur
	 */
	private String director;

	/**
	 * Casting
	 */
	private List<ActorBean> casting = new ArrayList<ActorBean>();

	/**
	 * Note presse
	 */
	private float pressRating;

	/**
	 * Note spectateurs
	 */
	private float userRating;

	/**
	 * Date de dernière mise à jour
	 */
	private Date lastUpdate;

	// GETTERS UTILITAIRES

	/**
	 * Renvoie l'URL de la jaquette (taille originale)
	 *
	 * @return String
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	public final String getUrlPosterOriginal() throws DataException, ConfigurationException {
		if (poster) {
			return ConfigProperties.getInstance().getPostersOriginalFilesPath() + provider + "_" + id + ".jpg";
		}
		return null;
	}

	/**
	 * Renvoie l'URL de la jaquette (taille miniature)
	 *
	 * @return String
	 * @throws DataException
	 * @throws ConfigurationException
	 */
	public final String getUrlPosterThumbnail() throws DataException, ConfigurationException {
		if (poster) {
			return ConfigProperties.getInstance().getPostersThumbnailFilesPath() + provider + "_" + id + ".jpg";
		}
		return null;
	}

	/**
	 * Renvoie le titre du film (éventuellement titre original)
	 *
	 * @return String
	 */
	public final String getTitle() {
		String result;
		if ((title != null) && StringUtils.isNotBlank(title)) {
			result = title;
			if ((titleOriginal != null) && StringUtils.isNotBlank(titleOriginal) && !StringUtils.equalsIgnoreCase(title, titleOriginal)) {
				result = result + " (" + titleOriginal + ")";
			}
		} else {
			result = titleOriginal;
		}
		return result;
	}

	/**
	 * Renvoie le titre du film (éventuellement titre original) concaténé avec
	 * l'identifiant. Méthode utilisée pour la comparaison
	 *
	 * @return String
	 */
	private String getTitleAndId() {
		return getTitle() + getId();
	}

	/**
	 * Renvoie le dossier contenant le film
	 *
	 * @return String
	 */
	public final String getFolder() {
		return FileUtils.getFileFolder(urlFile);
	}

	/**
	 * Renvoie la moyenne des notes
	 *
	 * @return Float
	 */
	public final float getPopularity() {
		if (userRating == 0) {
			if (pressRating == 0) {
				return 0;
			} else {
				return pressRating;
			}
		} else {
			if (pressRating == 0) {
				return userRating;
			} else {
				return ((userRating + pressRating) / 2);
			}
		}
	}

	/**
	 * Renvoie la moyenne des notes par tranche de 0.5
	 *
	 * @return Float
	 */
	public final float getPopularityForSort() {
		final float popularity = getPopularity();
		if (popularity == 0) {
			return popularity;
		} else if (popularity <= 0.5) {
			return (float) 0.5;
		} else if (popularity <= 1) {
			return 1;
		} else if (popularity <= 1.5) {
			return (float) 1.5;
		} else if (popularity <= 2) {
			return 2;
		} else if (popularity <= 2.5) {
			return (float) 2.5;
		} else if (popularity <= 3) {
			return 3;
		} else if (popularity <= 3.5) {
			return (float) 3.5;
		} else if (popularity <= 4) {
			return 4;
		} else if (popularity <= 4.5) {
			return (float) 4.5;
		} else {
			return 5;
		}
	}

	// GETTERS/SETTERS

	/**
	 * @return the provider
	 */
	public final Provider getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public final void setProvider(final Provider provider) {
		this.provider = provider;
	}

	/**
	 * @return the id
	 */
	public final Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public final void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the urlFile
	 */
	public final String getUrlFile() {
		return urlFile;
	}

	/**
	 * @param urlFile the urlFile to set
	 */
	public final void setUrlFile(final String urlFile) {
		this.urlFile = urlFile;
	}

	/**
	 * @return the dejaVu
	 */
	public final boolean isAlreadySeen() {
		return alreadySeen;
	}

	/**
	 * @param alreadySeen the dejaVu to set
	 */
	public final void setAlreadySeen(final boolean alreadySeen) {
		this.alreadySeen = alreadySeen;
	}

	/**
	 * @return the toSee
	 */
	public final boolean isToSee() {
		return toSee;
	}

	/**
	 * @param toSee the toSee to set
	 */
	public final void setToSee(final boolean toSee) {
		this.toSee = toSee;
	}

	/**
	 * @return the poster
	 */
	public final boolean isPoster() {
		return poster;
	}

	/**
	 * @param poster the jaquette to set
	 */
	public final void setPoster(final boolean poster) {
		this.poster = poster;
	}

	/**
	 * @return the urlPoster
	 */
	public final String getUrlPoster() {
		return urlPoster;
	}

	/**
	 * @param urlPoster the urlPoster to set
	 */
	public final void setUrlPoster(final String urlPoster) {
		this.urlPoster = urlPoster;
	}

	/**
	 * @param title the titre to set
	 */
	public final void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * @param titleOriginal the titreOriginal to set
	 */
	public final void setTitleOriginal(final String titleOriginal) {
		this.titleOriginal = titleOriginal;
	}

	/**
	 * @return the resume
	 */
	@Deprecated
	public final String getResume() {
		return resume;
	}

	/**
	 * @param resume the resume to set
	 */
	@Deprecated
	public final void setResume(String resume) {
		this.resume = resume;
	}

	/**
	 * @return the synopsis
	 */
	public final String getSynopsis() {
		return synopsis;
	}

	/**
	 * @param synopsis the synopsis to set
	 */
	public final void setSynopsis(final String synopsis) {
		this.synopsis = synopsis;
	}

	/**
	 * @return the genres
	 */
	public final List<String> getGenres() {
		return genres;
	}

	/**
	 * @param genres the genres to set
	 */
	public final void setGenres(final List<String> genres) {
		this.genres = genres;
	}

	/**
	 * @return the anneeProd
	 */
	public final String getYear() {
		return year;
	}

	/**
	 * @param year the anneeProd to set
	 */
	public final void setYear(final String year) {
		this.year = year;
	}

	/**
	 * @return the urlTrailer
	 */
	public final String getUrlTrailer() {
		return urlTrailer;
	}

	/**
	 * @param urlTrailer the urlTrailer to set
	 */
	public final void setUrlTrailer(final String urlTrailer) {
		this.urlTrailer = urlTrailer;
	}

	/**
	 * @return the nationalites
	 */
	public final List<String> getNationalities() {
		return nationalities;
	}

	/**
	 * @param nationalities the nationalites to set
	 */
	public final void setNationalities(final List<String> nationalities) {
		this.nationalities = nationalities;
	}

	/**
	 * @return the realisateur
	 */
	public final String getDirector() {
		return director;
	}

	/**
	 * @param director the realisateur to set
	 */
	public final void setDirector(final String director) {
		this.director = director;
	}

	/**
	 * @return the casting
	 */
	public final List<ActorBean> getCasting() {
		return casting;
	}

	/**
	 * @param casting the casting to set
	 */
	public final void setCasting(final List<ActorBean> casting) {
		this.casting = casting;
	}

	/**
	 * @return the infosCompl
	 */
	public final String getOtherInfos() {
		return otherInfos;
	}

	/**
	 * @param otherInfos the infosCompl to set
	 */
	public final void setOtherInfos(final String otherInfos) {
		this.otherInfos = otherInfos;
	}

	/**
	 * @return the notePresse
	 */
	public final float getPressRating() {
		return pressRating;
	}

	/**
	 * @param pressRating the notePresse to set
	 */
	public final void setPressRating(final float pressRating) {
		this.pressRating = pressRating;
	}

	/**
	 * @return the noteSpectateurs
	 */
	public final float getUserRating() {
		return userRating;
	}

	/**
	 * @param userRating the noteSpectateurs to set
	 */
	public final void setUserRating(final float userRating) {
		this.userRating = userRating;
	}

	/**
	 * @return the derniereMiseAJour
	 */
	public final Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param lastUpdate the derniereMiseAJour to set
	 */
	public final void setLastUpdate(final Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @return the langue
	 */
	public final Language getLangue() {
		return language;
	}

	/**
	 * @param language the langue to set
	 */
	public final void setLangue(final Language language) {
		this.language = language;
	}

	@Override
	public int compareTo(final MovieBean movieToCompare) {
		return getTitleAndId().compareTo(movieToCompare.getTitleAndId());
	}

	@Override
	public int hashCode() {
		return getTitleAndId().hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final MovieBean movie = (MovieBean) obj;
		if (getTitleAndId() == null) {
			if (movie.getTitleAndId() != null) {
				return false;
			}
		} else if (!getTitleAndId().equals(movie.getTitleAndId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return getTitle();
	}

}
