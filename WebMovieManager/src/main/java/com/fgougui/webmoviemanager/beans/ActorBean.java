package com.fgougui.webmoviemanager.beans;

import java.io.Serializable;

/**
 * Acteur de film
 */
@SuppressWarnings("serial")
public class ActorBean implements Serializable {

	/**
	 * Nom
	 */
	private String name;

	/**
	 * Personnage joué
	 */
	private String role;

	/**
	 * 
	 */
	public ActorBean(final String nom, final String role) {
		name = nom;
		this.role = role;
	}

	/**
	 * @return the nom
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param nom
	 * the nom to set
	 */
	public final void setName(final String nom) {
		name = nom;
	}

	/**
	 * @return the role
	 */
	public final String getRole() {
		return role;
	}

	/**
	 * @param role
	 * the role to set
	 */
	public final void setRole(final String role) {
		this.role = role;
	}

}
