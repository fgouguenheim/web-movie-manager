package com.fgougui.webmoviemanager.beans;

import com.fgougui.webmoviemanager.utils.EncodingUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.*;

/**
 * Collection de films
 */
@SuppressWarnings("serial")
public class MovieManagerBean implements Serializable {

	/** Films de la filmotheque */
	private final Set<MovieBean> movies = new TreeSet<MovieBean>();

	/** Genres de la filmotheque (non stocké sur disque car généré à la volée) */
	private transient Map<String, Integer> genres;

	/** Dossiers de la filmotheque (non stocké sur disque car généré à la volée) */
	private transient Map<String, Integer> folders;

	/** Ensemble des notes moyennes des films (non stocké sur disque car généré à la volée) */
	private transient Map<Float, Integer> popularities;

	/** Ensemble des url des fichiers à renseigner (non stocké sur disque car généré à la volée) */
	private transient Set<String> urlsFilesToFill;

	/** Ensemble des url des fichiers n'étant pas des films */
	private final Set<String> urlsFilesNotMovies = new HashSet<String>();

	/**
	 * Renvoie la liste triée de tous les films
	 * 
	 * @return List
	 */
	public final Collection<MovieBean> getMovies() {
		return movies;
	}

	/**
	 * Renvoie un film en fonction de son identifiant
	 * 
	 * @return MovieBean
	 */
	public final MovieBean getMovie(final long id) {
		for (final MovieBean movie : movies) {
			if (id == movie.getId()) {
				return movie;
			}
		}
		return null;
	}

	/**
	 * Renvoie la liste des films correspondant à un genre donné Si genre est vide ou nul, renvoie la liste de tous les
	 * films
	 * 
	 * @param genre
	 * String
	 * @return List
	 */
	public final Collection<MovieBean> getMoviesByGenre(final String genre) {
		// Filtre
		if ((genre == null) || genre.isEmpty()) {
			return movies;
		}
		final Set<MovieBean> result = new TreeSet<MovieBean>();
		for (final MovieBean movie : movies) {
			if (movie.getGenres().contains(genre)) {
				result.add(movie);
			}
		}
		return result;
	}

	/**
	 * Renvoie la liste des films correspondant à un dossier donné. Si dossier est vide ou nul, renvoie la liste de tous
	 * les
	 * films.
	 * 
	 * @param genre
	 * String
	 * @return List
	 */
	public final Collection<MovieBean> getMoviesByFolder(final String folder) {
		// Filtre
		if ((folder == null) || folder.isEmpty()) {
			return movies;
		}
		final Set<MovieBean> result = new TreeSet<MovieBean>();
		for (final MovieBean movie : movies) {
			if (movie.getFolder().equals(folder)) {
				result.add(movie);
			}
		}
		return result;
	}

	/**
	 * Renvoie la liste triée des films correspondant à un pattern donné.
	 * 
	 * @param pattern
	 * Pattern de recherche
	 * @return List
	 */
	public final Collection<MovieBean> getMoviesByPattern(String pattern) {
		// Supprime les accents du pattern
		pattern = EncodingUtils.removeAccents(pattern);
		final Set<MovieBean> result = new TreeSet<MovieBean>();
		for (final MovieBean movieBean : getMovies()) {
			if (StringUtils.containsIgnoreCase(EncodingUtils.removeAccents(movieBean.getTitle()), pattern)) {
				result.add(movieBean);
			}
		}
		return result;
	}

	/**
	 * Renvoie (et éventuellement génère) la liste de tous les genres mappés avec le nombre d'éléments contenus
	 * 
	 * @return List
	 */
	public final Map<String, Integer> getGenres() {
		// TODO : Gérer la persistance de cette valeur
		// if (genres == null) {
		genres = new TreeMap<String, Integer>();
		for (final MovieBean movie : movies) {
			for (final String genre : movie.getGenres()) {
				if (genres.containsKey(genre)) {
					final Integer previousVal = genres.get(genre);
					final Integer nextVal = previousVal + 1;
					genres.put(genre, nextVal);
				} else {
					genres.put(genre, 1);
				}
			}
		}
		// }
		return genres;
	}

	/**
	 * Renvoie (et éventuellement génère) la liste de tous les dossiers mappés avec le nombre d'éléments contenus
	 * 
	 * @return List
	 */
	public final Map<String, Integer> getFolders() {
		// TODO : Gérer la persistance de cette valeur
		// if (folders == null) {
		folders = new TreeMap<String, Integer>();
		for (final MovieBean movie : movies) {
			final String folder = movie.getFolder();
			if (folders.containsKey(folder)) {
				final Integer previousVal = folders.get(folder);
				final Integer nextVal = previousVal + 1;
				folders.put(folder, nextVal);
			} else {
				folders.put(folder, 1);
			}
		}
		// }
		return folders;
	}

	/**
	 * Renvoie (et éventuellement génère) la liste de toutes les notes moyennes mappés avec le nombre d'éléments
	 * contenus
	 * 
	 * @return List
	 */
	public final Map<Float, Integer> getPopularities() {
		// TODO : Gérer la persistance de cette valeur
		// if (popularities == null) {
		popularities = new TreeMap<Float, Integer>();
		for (final MovieBean movie : movies) {
			final Float popularity = movie.getPopularityForSort();
			if (popularities.containsKey(popularity)) {
				final Integer previousVal = popularities.get(popularity);
				final Integer nextVal = previousVal + 1;
				popularities.put(popularity, nextVal);
			} else {
				popularities.put(popularity, 1);
			}
		}
		// }
		return popularities;
	}

	/**
	 * Renvoie la liste des urls des fichiers à trier
	 * 
	 * @return List
	 */
	public final Set<String> getUrlsFilesToFill() {
		if (urlsFilesToFill == null) {
			urlsFilesToFill = new HashSet<String>();
		}
		return urlsFilesToFill;
	}

	/**
	 * Renvoie la liste des urls des fichiers n'étant pas des films
	 * 
	 * @return List
	 */
	public final Set<String> getUrlsFilesNotMovies() {
		return urlsFilesNotMovies;
	}

	@Override
	public String toString() {
		return movies.toString();
	}

}
