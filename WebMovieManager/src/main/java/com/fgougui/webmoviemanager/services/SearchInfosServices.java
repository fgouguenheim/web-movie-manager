package com.fgougui.webmoviemanager.services;

import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.config.ConfigProperties;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.exceptions.FunctionalException;
import com.fgougui.webmoviemanager.services.strategies.SearchInfosStrategy;
import com.fgougui.webmoviemanager.utils.FileUtils;
import com.fgougui.webmoviemanager.utils.GraphicsUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Date;
import java.util.Set;

import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.*;

/**
 * Services pour la recherche d'informations sur un film
 */
@Service
public class SearchInfosServices {

	/**
	 * Stratégie de récupération des infos
	 */
	@Autowired
	@Qualifier("theMovieDBStrategy")
	private SearchInfosStrategy searchInfosStrategy;

	@PostConstruct
	private void init() {
		try {
			// Initialisation des paramètres de réseau
			final boolean isProxy = ConfigProperties.getInstance().isNetworkProxy();
			// Gestion du proxy
			if (isProxy) {
				final String proxyHost = ConfigProperties.getInstance().getNetworkProxyHost();
				final String proxyPort = ConfigProperties.getInstance().getNetworkProxyPort();
				final String proxyLogin = ConfigProperties.getInstance().getNetworkProxyLogin();
				final String proxyPassword = ConfigProperties.getInstance().getNetworkProxyPassword();
				System.getProperties().setProperty("proxySet", "true");
				System.getProperties().setProperty("proxyHost", proxyHost);
				System.getProperties().setProperty("proxyPort", proxyPort);
				Authenticator.setDefault(new Authenticator() {

					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(proxyLogin, proxyPassword.toCharArray());
					}
				});
			}
			// Initialisation de la stratégie
			searchInfosStrategy.init();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Effectue une recherche sur le site AlloCine
	 * 
	 * @param movieName
	 * Nom du film
	 * @return resultats
	 * @throws FunctionalException
	 */
	public Set<MovieBean> findMovie(final String movieName) throws FunctionalException, IOException {
		// Appel à la stratégie
		return searchInfosStrategy.findMovie(movieName);
	}

	/**
	 * Remplit les informations sur le film à partir du site AlloCine
	 * 
	 * @param movieManagerBean
	 * Filmotheque
	 * @param String
	 * Url du fichier à renseigner
	 * @param idMovie
	 * Identifiant du film
	 * @return MovieBean Le film ajouté
	 * @throws FunctionalException
	 * @throws IOException
	 * @throws DataException
	 */
	public MovieBean fillMovie(final MovieManagerBean movieManagerBean, final String urlFileToFill, final String idMovie,
			final boolean toSee, final boolean alreadySeen) throws FunctionalException, IOException, DataException {

		// Récupération de l'identifiant
		long id;
		try {
			id = Long.parseLong(idMovie);
		} catch (final Exception e) {
			throw new FunctionalException(EXCEPTION_ID_NOT_INTEGER, e);
		}

		// On vérifie si le film à remplir existe déjà dans la filmothèque et que son URL est différente
		// => dans ce cas, il s'agit d'un doublon
		if ((movieManagerBean.getMovie(id) != null) && !StringUtils.equals(movieManagerBean.getMovie(id).getUrlFile(), urlFileToFill)) {
			throw new FunctionalException(EXCEPTION_MOVIE_DOUBLE + EXCEPTION_MOVIE_DOUBLE_URL_PRESENT
					+ movieManagerBean.getMovie(id).getUrlFile() + EXCEPTION_MOVIE_DOUBLE_URL_NEW + urlFileToFill);
		}

		// Appel à la stratégie
		final MovieBean movieBean = searchInfosStrategy.fillMovie(movieManagerBean, urlFileToFill, id, toSee, alreadySeen);

		// Téléchargement de la jaquette
		downloadPoster(movieBean);

		// Renommage éventuel du fichier
		if (ConfigProperties.getInstance().isMovieManagerAutoRename()) {
			final String movieTitle = movieBean.getTitle();
			final String moviePath = FileUtils.getFilePath(urlFileToFill);
			final String extension = FileUtils.getFileExtension(urlFileToFill);
			final String newUrl = moviePath + movieTitle + extension;
			final File oldFile = new File(urlFileToFill);
			final File newFile = new File(newUrl);
			final boolean ok = oldFile.renameTo(newFile);
			if (ok) {
				movieBean.setUrlFile(newUrl);
			}
		}

		// On renseigne la date de dernière mise à jour
		movieBean.setLastUpdate(new Date());

		// Ajout du film dans la filmotheque
		movieManagerBean.getMovies().add(movieBean);
		// On supprime l'url de la liste des url à renseigner
		movieManagerBean.getUrlsFilesToFill().remove(urlFileToFill);

		// Renvoi du film renseigné
		return movieBean;
	}

	/**
	 * Met à jour les critiques sur le film à partir du site AlloCine
	 * 
	 * @param movieBean
	 * MovieBean
	 * @throws FunctionalException
	 * @throws IOException
	 * @throws DataException
	 */
	public void updateMovie(final MovieBean movieBean) throws FunctionalException, IOException, DataException {

		// Appel à la stratégie
		searchInfosStrategy.updateMovie(movieBean);

		// Téléchargement de la jaquette si nécessaire
		if (!movieBean.isPoster() && StringUtils.isNotBlank(movieBean.getUrlPoster())) {
			downloadPoster(movieBean);
		}
	}

	/**
	 * Télécharge une jaquette depuis internet à partir de son URL et ajoute l'info dans le MovieBean
	 * 
	 * @param movieBean Un movieBean dont l'id et l'urlPoster sont déjà renseignés
	 * @throws DataException
	 */
	private void downloadPoster(final MovieBean movieBean) throws DataException {
		movieBean.setPoster(true);
		// Téléchargement de l'affiche
		InputStream inputStream = null;
		try {
			inputStream = new URL(movieBean.getUrlPoster()).openStream();
			if (inputStream != null) {
				try {
					// Si l'image est déjà présente sur le disque, on l'écrase
					final String pictureFileName = movieBean.getUrlPosterOriginal();
					final File pictureFile = new File(pictureFileName);
					if (pictureFile.exists()) {
						pictureFile.delete();
					}
					pictureFile.getParentFile().mkdirs();
					// Enregistrement de l'image sur le disque
					final FileOutputStream fileOutputStream = new FileOutputStream(pictureFileName);
					IOUtils.copy(inputStream, fileOutputStream);
					IOUtils.closeQuietly(inputStream);
					IOUtils.closeQuietly(fileOutputStream);

					// Réduction de l'image
					final String thumbnailFileName = movieBean.getUrlPosterThumbnail();
					final File thumbnailFile = new File(thumbnailFileName);
					if (thumbnailFile.exists()) {
						thumbnailFile.delete();
					}
					thumbnailFile.getParentFile().mkdirs();

					GraphicsUtils.createJPGThumbnail(pictureFileName, thumbnailFileName);
				} catch (final Exception e) {
					// IOException
					throw new DataException(EXCEPTION_FILE_POSTER, e);
				}
			}
		} catch (final IOException e) {
			// Finalement, pas de jaquette...
			movieBean.setPoster(false);
		}
	}

	/**
	 * Signale un fichier comme n'étant pas un film
	 * 
	 * @param movieManagerBean
	 * MovieManagerBean
	 * @param urlFileToReport
	 * String
	 */
	public void reportFileIsNotAMovie(final MovieManagerBean movieManagerBean, final String urlFileToReport) {
		movieManagerBean.getUrlsFilesToFill().remove(urlFileToReport);
		movieManagerBean.getUrlsFilesNotMovies().add(urlFileToReport);
	}

}
