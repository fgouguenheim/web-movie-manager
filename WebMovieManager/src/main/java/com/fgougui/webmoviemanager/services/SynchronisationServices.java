package com.fgougui.webmoviemanager.services;

import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieBean.Provider;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.config.ConfigProperties;
import com.fgougui.webmoviemanager.dao.MovieManagerDao;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.exceptions.FunctionalException;
import com.fgougui.webmoviemanager.utils.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Services concernant la synchronisation des données avec les fichiers présents
 * sur le disque
 */
@Service
public class SynchronisationServices {

	/** Liste des noms des fichiers à synchroniser */
	private List<String> urlFilesToSynchronise;

	/** Extensions à ignorer */
	private List<String> extensionsToIgnore;

	/**
	 * Synchronise la filmotheque avec les fichiers présents dans les
	 * répertoires fournis en paramètres
	 * 
	 * @param movieManagerBean
	 * @param folders
	 * @param filesExtensionToIgnore
	 * @throws FunctionalException
	 * @throws ConfigurationException
	 * @throws DataException
	 */
	public void synchronise(final MovieManagerBean movieManagerBean, final String[] folders, final String[] filesExtensionToIgnore)
			throws FunctionalException, DataException, ConfigurationException {

		// Construction de la liste des films sur le disque
		extensionsToIgnore = Arrays.asList(filesExtensionToIgnore);
		buildFilesToSynchroniseList(folders);

		// Pour chaque fichier de la filmotheque...
		final Iterator<MovieBean> it = movieManagerBean.getMovies().iterator();
		while (it.hasNext()) {
			final MovieBean movie = it.next();
			// On vérifie qu'il est encore sur le disque
			final String urlFile = movie.getUrlFile();
			final File file = new File(urlFile);
			// S'il n'est pas sur le disque à l'url indiquée, on regarde s'il
			// est à un autre endroit sur le disque
			if (!file.exists()) {
				boolean isFileFound = false;
				// Pour chaque fichier sur le disque
				for (final String urlFileToSynchronise : urlFilesToSynchronise) {
					// On compare au fichier de la filmotheque
					// TODO : Améliorer la comparaison du nom
					// Si c'est le même nom, on lie le film à la nouvelle url
					if (FileUtils.getFileNameWithExtension(urlFileToSynchronise).equalsIgnoreCase(
							FileUtils.getFileNameWithExtension(urlFile))) {
						movie.setUrlFile(urlFileToSynchronise);
						isFileFound = true;
						// Log
						MovieManagerDao.appendToMovieManagerLogs("Le film " + movie.getTitle() + " a été déplacé de '" + urlFile
								+ "' vers '" + urlFileToSynchronise + "'");
						break;
					}
				}
				// S'il n'est pas à un autre endroit sur le disque, on le
				// supprime de la filmotheque
				if (!isFileFound) {
					it.remove();
					// Log
					MovieManagerDao.appendToMovieManagerLogs("Le film " + movie.getTitle()
							+ " n'existe plus sur le disque. Il est supprimé.");
				}
			}
			// ****************************************************************************************
			// TODO : A supprimer une fois la filmothèque mise à jour
			// S'il n'a pas de provider, on doit sette le provider AlloCine
			// (en effet, il n'existait à l'origine qu'un seul provider : Allociné)
			if (movie.getProvider() == null) {
				movie.setProvider(Provider.ALLOCINE);
				// Log
				MovieManagerDao.appendToMovieManagerLogs("Le film " + movie.getTitle()
						+ " n'avait pas de provider. On lui sette le provider AlloCine");
				// On renomme ses jaquette en conséquence
				if (movie.isPoster()) {
					final String oldUrlPosterOriginal = ConfigProperties.getInstance().getPostersOriginalFilesPath() + movie.getId()
							+ ".jpg";
					final String oldUrlPosterThumbnail = ConfigProperties.getInstance().getPostersThumbnailFilesPath() + movie.getId()
							+ ".jpg";
					final File oldPosterOriginalFile = new File(oldUrlPosterOriginal);
					final File oldPosterThumbnailFile = new File(oldUrlPosterThumbnail);
					if (oldPosterOriginalFile.exists() && oldPosterThumbnailFile.exists()) {
						try {
							// Renommage des fichiers
							final File newPosterOriginalFile = new File(movie.getUrlPosterOriginal());
							final File newPosterThumbnailFile = new File(movie.getUrlPosterThumbnail());
							final FileInputStream oldPosterOriginalInputStream = new FileInputStream(oldPosterOriginalFile);
							final FileOutputStream newPosterOriginalInputStream = new FileOutputStream(newPosterOriginalFile);
							final FileInputStream oldPosterThumbnailInputStream = new FileInputStream(oldPosterThumbnailFile);
							final FileOutputStream newPosterThumbnailInputStream = new FileOutputStream(newPosterThumbnailFile);
							IOUtils.copy(oldPosterOriginalInputStream, newPosterOriginalInputStream);
							IOUtils.copy(oldPosterThumbnailInputStream, newPosterThumbnailInputStream);
							IOUtils.closeQuietly(oldPosterOriginalInputStream);
							IOUtils.closeQuietly(newPosterOriginalInputStream);
							IOUtils.closeQuietly(oldPosterThumbnailInputStream);
							IOUtils.closeQuietly(newPosterThumbnailInputStream);
							final boolean isOldOriginalPosterDeleted = oldPosterOriginalFile.delete();
							final boolean isOldThumbnailPosterDeleted = oldPosterThumbnailFile.delete();
							// Log
							MovieManagerDao.appendToMovieManagerLogs("  -> on renomme ses jaquette pour le provider AlloCine");
							if (!isOldOriginalPosterDeleted | !isOldThumbnailPosterDeleted) {
								MovieManagerDao
										.appendToMovieManagerLogs("  -> attention : impossible de renommer les jaquettes originales");
							}
						} catch (final Exception e) {
							// Log
							MovieManagerDao.appendToMovieManagerLogs("  -> impossible de renommer les jaquettes : " + e.getMessage());
						}
					}
				}
			}
			// ****************************************************************************************
		}

		// Construction de la liste des fichiers contenus dans la filmotheque
		final List<String> urlMovieManagerFiles = buildMovieManagerFilesList(movieManagerBean);

		// Pour chaque fichier sur le disque, on regarde s'il est déjà dans la
		// filmotheque ou dans la liste des fichiers signalés comme n'étant
		// pas des films
		for (final String urlFileToSynchronise : urlFilesToSynchronise) {
			// S'il n'y est pas, on l'ajoute en tant que fichier à trier
			if (!urlMovieManagerFiles.contains(urlFileToSynchronise)
					&& !movieManagerBean.getUrlsFilesNotMovies().contains(urlFileToSynchronise)) {
				movieManagerBean.getUrlsFilesToFill().add(urlFileToSynchronise);
			}
		}
	}

	private List<String> buildMovieManagerFilesList(final MovieManagerBean movieManagerBean) {
		final List<String> result = new ArrayList<String>();
		for (final MovieBean movie : movieManagerBean.getMovies()) {
			result.add(movie.getUrlFile());
		}
		return result;
	}

	private void buildFilesToSynchroniseList(final String[] urlsFolders) throws FunctionalException {
		urlFilesToSynchronise = new ArrayList<String>();
		// Pour chacun des répertoires, on liste tous les fichiers qu'il
		// contient
		for (final String urlFolder : urlsFolders) {
			final File folder = new File(urlFolder);
			if (!folder.isDirectory()) {
				throw new FunctionalException("Le répertoire '" + urlFolder + "' de la filmothèque est incorrect.");
			}
			listFolder(folder);
		}
	}

	private void listFolder(final File folderOrFile) {
		if (folderOrFile.isDirectory() && !isHiddenFile(folderOrFile.getName())) {
			final File[] list = folderOrFile.listFiles();
			if (list != null) {
				for (final File element : list) {
					// Appel récursif sur les sous-répertoires
					listFolder(element);
				}
			}
		} else if (folderOrFile.isFile() && !ignoreExtension(folderOrFile.getName()) && !isHiddenFile(folderOrFile.getName())) {
			urlFilesToSynchronise.add(folderOrFile.getAbsolutePath());
		}
	}

	private boolean ignoreExtension(final String fileName) {
		for (final String extensionToIgnore : extensionsToIgnore) {
			if (StringUtils.endsWithIgnoreCase(fileName, extensionToIgnore)) {
				return true;
			}
		}
		return false;
	}

	private boolean isHiddenFile(final String fileName) {
		return StringUtils.startsWith(fileName, ".");
	}

}
