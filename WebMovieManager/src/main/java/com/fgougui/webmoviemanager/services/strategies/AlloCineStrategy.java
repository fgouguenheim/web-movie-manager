/**
 * 
 */
package com.fgougui.webmoviemanager.services.strategies;

import com.fgougui.webmoviemanager.beans.ActorBean;
import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieBean.Provider;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.exceptions.FunctionalException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Pattern;

import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.EXCEPTION_PARSE_JSON;

/**
 * Stratégie de récupération des infos AlloCine
 */
@Component
public class AlloCineStrategy implements SearchInfosStrategy {

	/** Numéro de partenaire allociné */
	private static final String ALLOCINE_PARTNER_NUMBER = "V2luZG93czg";

	/** Clé "secrète" allociné */
	private static final String ALLOCINE_SECRET_KEY = "e2b7fd293906435aa5dac4be670e7982";

	/** URL de l'API AlloCine */
	private static final String ALLOCINE_API_URL = "http://api.allocine.fr/rest/v3";

	/** Formatteur de date pour l'API AlloCine */
	private static final DateTimeFormatter SED_FORMATTER = DateTimeFormat.forPattern("YYYYMMdd");

	@Override
	public void init() throws Exception {
	}

	/**
	 * @param url
	 * @return
	 * @throws IOException
	 */
	private String doRequestURL(final String method, final LinkedHashMap<String, String> params) throws IOException {

		// Ajoute le paramètre "format=json"
		params.put("format", "json");

		// Ajoute le paramètre "partner"
		params.put("partner", ALLOCINE_PARTNER_NUMBER);

		// Construit le paramètre "sed" et l'ajoute à la liste de paramètres
		final String sed = new DateTime().toString(SED_FORMATTER);
		params.put("sed", sed);

		// Construit le paramètre "sid" et l'ajoute à la liste de paramètres
		final String sid = URLEncoder.encode(DigestUtils.sha1Hex(ALLOCINE_SECRET_KEY + toQueryString(params)), "UTF-8");
		params.put("sid", sid);

		// Construit l'URL et envoie la requête
		final URL url = new URL(ALLOCINE_API_URL + "/" + method + "?" + toQueryString(params));
		return IOUtils.toString(url);
	}

	/**
	 * @param params
	 * @return
	 * @throws IOException
	 */
	private String toQueryString(final Map<String, String> params) throws IOException {
		final StringBuffer queryString = new StringBuffer();
		for (final Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator(); iterator.hasNext();) {
			final Map.Entry<String, String> param = iterator.next();
			queryString.append(param.getKey());
			queryString.append("=");
			queryString.append(URLEncoder.encode(param.getValue(), "UTF-8"));
			if (iterator.hasNext()) {
				queryString.append("&");
			}
		}
		return queryString.toString();
	}

	@Override
	public Set<MovieBean> findMovie(final String movieName) throws FunctionalException, IOException {

		// Modification du nom du film : on supprime le "le", "la" ou "l'" du début du nom (meilleure recherche sur AlloCine)
		String movieNameQuery = movieName;
		movieNameQuery = StringUtils.removeStartIgnoreCase(movieNameQuery, "le ");
		movieNameQuery = StringUtils.removeStartIgnoreCase(movieNameQuery, "la ");
		movieNameQuery = StringUtils.removeStartIgnoreCase(movieNameQuery, "l'");

		// Correctif bug sur le mot "declare" (??) => on remplace par "déclare"
		if (StringUtils.containsIgnoreCase(movieNameQuery, "declare")) {
			movieNameQuery = Pattern.compile("declare", Pattern.CASE_INSENSITIVE).matcher(movieNameQuery).replaceAll("déclare");
		}

		// Resultats de la recherche d'un film
		final Set<MovieBean> movies = new LinkedHashSet<MovieBean>();

		// Construction de l'URL et lancement de la requête
		final LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
		params.put("q", movieNameQuery);
		final String response = doRequestURL("search", params);

		// Parsage du contenu et construction des résultats
		JSONObject json;
		try {
			json = new JSONObject(response);
			final JSONObject root = json.getJSONObject("feed");
			JSONArray moviesJson;
			try {
				moviesJson = root.getJSONArray("movie");
			} catch (final JSONException e) {
				moviesJson = new JSONArray();
			}
			for (int i = 0; i < moviesJson.length(); i++) {
				final JSONObject movieJson = moviesJson.getJSONObject(i);
				// Identifiant du film
				final String id = movieJson.getString("code");
				// Titre
				String title = null;
				try {
					title = movieJson.getString("title");
				} catch (final JSONException e) {
					// Pas de titre...
				}
				final String titleOriginal = movieJson.getString("originalTitle");
				// Année de production
				String year = null;
				try {
					year = movieJson.getString("productionYear");
				} catch (final JSONException e) {
					// Pas de "productionYear"...
					try {
						final String releaseDate = movieJson.getJSONObject("release").getString("releaseDate");
						year = StringUtils.substring(releaseDate, 0, 4);
					} catch (final JSONException e1) {
						// Pas possible de déterminer l'année...
					}
				}
				// URL de l'affiche
				JSONObject poster;
				String urlPoster = null;
				try {
					poster = movieJson.getJSONObject("poster");
					urlPoster = poster.getString("href");
				} catch (final JSONException e) {
					// Pas de poster...
				}
				// Construction du film résultat
				final MovieBean movie = new MovieBean();
				movie.setId(Long.parseLong(id));
				movie.setTitle(title);
				movie.setTitleOriginal(titleOriginal);
				movie.setUrlPoster(urlPoster);
				movie.setYear(year);
				movies.add(movie);
			}
		} catch (final JSONException e) {
			throw new FunctionalException(EXCEPTION_PARSE_JSON, e);
		}

		return movies;
	}

	@Override
	public MovieBean fillMovie(final MovieManagerBean movieManagerBean, final String urlFileToFill, final long id, final boolean toSee,
			final boolean alreadySeen) throws FunctionalException, IOException, DataException {

		// Le film à remplir
		final MovieBean movieBean = new MovieBean();
		movieBean.setId(id);
		movieBean.setUrlFile(urlFileToFill);
		movieBean.setToSee(toSee);
		movieBean.setAlreadySeen(alreadySeen);
		movieBean.setProvider(Provider.ALLOCINE);

		// Construction de l'URL et lancement de la requête
		final LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
		params.put("code", movieBean.getId().toString());
		final String response = doRequestURL("movie", params);

		// Parsage du contenu et construction des résultats
		JSONObject json;
		try {
			json = new JSONObject(response);
			final JSONObject movie = json.getJSONObject("movie");
			// Récupération des infos

			// Titre
			String title = null;
			try {
				title = movie.getString("title");
			} catch (final JSONException e) {
				// Pas de titre...
			}
			final String titleOriginal = movie.getString("originalTitle");
			movieBean.setTitle(title);
			movieBean.setTitleOriginal(titleOriginal);

			// Année de production
			String year = null;
			try {
				year = movie.getString("productionYear");
			} catch (final JSONException e) {
				// Pas de "productionYear"...
				try {
					final String releaseDate = movie.getJSONObject("release").getString("releaseDate");
					year = StringUtils.substring(releaseDate, 0, 4);
				} catch (final JSONException e1) {
					// Pas possible de déterminer l'année...
				}
			}
			movieBean.setYear(year);

			// Nationalités
			try {
				final JSONArray nationalitiesArray = movie.getJSONArray("nationality");
				final List<String> nationalities = new ArrayList<String>();
				for (int i = 0; i < nationalitiesArray.length(); i++) {
					final String nationality = nationalitiesArray.getJSONObject(i).getString("$");
					nationalities.add(nationality);
				}
				movieBean.setNationalities(nationalities);
			} catch (final JSONException e) {
				// Pas de nationalités...
			}

			// Genres
			try {
				final JSONArray genresArray = movie.getJSONArray("genre");
				final List<String> genres = new ArrayList<String>();
				for (int i = 0; i < genresArray.length(); i++) {
					final String genre = genresArray.getJSONObject(i).getString("$");
					genres.add(genre);
				}
				movieBean.setGenres(genres);
			} catch (final JSONException e) {
				// Pas de genres...
			}

			// Synopsis
			try {
				final String synopsis = movie.getString("synopsis");
				movieBean.setSynopsis(synopsis);
			} catch (final JSONException e) {
				// Pas de synopsis...
			}

			// Casting & réalisateur
			final List<ActorBean> casting = new ArrayList<ActorBean>();
			String director = null;
			final JSONArray castingList = movie.getJSONArray("castMember");
			for (int j = 0; j < castingList.length(); j++) {
				final JSONObject castMember = castingList.getJSONObject(j);
				final String person = castMember.getJSONObject("person").getString("name");
				final String roleCode = castMember.getJSONObject("activity").getString("code");
				if ("8001".equalsIgnoreCase(roleCode) || "8001".equalsIgnoreCase(roleCode)) {
					String role = null;
					try {
						role = castMember.getString("role");
					} catch (final JSONException e) {
						// Pas de role...
					}
					final ActorBean acteur = new ActorBean(person, role);
					casting.add(acteur);
				} else if ("8002".equalsIgnoreCase(roleCode)) {
					director = person;
				}
			}
			movieBean.setCasting(casting);
			movieBean.setDirector(director);

			// Informations complémentaires
			try {
				final String movieCertificate = movie.getJSONObject("movieCertificate").getString("$");
				movieBean.setOtherInfos(movieCertificate);
			} catch (final JSONException e) {
				// Pas d'nformations complémentaires...
			}

			// URL de l'affiche
			JSONObject poster;
			String urlPoster = null;
			try {
				poster = movie.getJSONObject("poster");
				urlPoster = poster.getString("href");
				movieBean.setUrlPoster(urlPoster);
			} catch (final JSONException e) {
				// Pas de poster...
			}

			// URL de la bande annonce
			try {
				final String urlTrailer = movie.getJSONObject("trailer").getString("href");
				movieBean.setUrlTrailer(urlTrailer);
			} catch (final JSONException e) {
				// Pas de bande annonce...
			}

			// Notes presse/spectateurs
			try {
				final JSONObject statistics = movie.getJSONObject("statistics");
				float pressRating = 0;
				float userRating = 0;
				try {
					pressRating = Float.parseFloat(statistics.getString("pressRating"));
				} catch (final Exception e) {
					// Pas de note...
				}
				try {
					userRating = Float.parseFloat(statistics.getString("userRating"));
				} catch (final Exception e) {
					// Pas de note...
				}
				movieBean.setPressRating(pressRating);
				movieBean.setUserRating(userRating);
			} catch (final JSONException e) {
				// Pas de notes...
			}

		} catch (final JSONException e) {
			throw new FunctionalException(EXCEPTION_PARSE_JSON, e);
		}

		return movieBean;
	}

	@Override
	public void updateMovie(final MovieBean movieBean) throws FunctionalException, IOException, DataException {

		// Construction de l'URL et lancement de la requête
		final LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
		params.put("code", movieBean.getId().toString());
		final String response = doRequestURL("movie", params);

		// Parsage du contenu et construction des résultats
		JSONObject json;
		try {
			json = new JSONObject(response);
			final JSONObject movie = json.getJSONObject("movie");
			// Récupération des infos

			// URL de l'affiche (s'il n'y a pas d'affiche)
			if (!movieBean.isPoster()) {
				JSONObject poster;
				String urlPoster = null;
				try {
					poster = movie.getJSONObject("poster");
					urlPoster = poster.getString("href");
					movieBean.setUrlPoster(urlPoster);
				} catch (final JSONException e) {
					// Pas de poster...
				}
			}

			// Notes presse/spectateurs
			try {
				final JSONObject statistics = movie.getJSONObject("statistics");
				float pressRating = 0;
				float userRating = 0;
				try {
					pressRating = Float.parseFloat(statistics.getString("pressRating"));
				} catch (final Exception e) {
					// Pas de note...
				}
				try {
					userRating = Float.parseFloat(statistics.getString("userRating"));
				} catch (final Exception e) {
					// Pas de note...
				}
				movieBean.setPressRating(pressRating);
				movieBean.setUserRating(userRating);
			} catch (final JSONException e) {
				// Pas de notes...
			}
		} catch (final JSONException e) {
			throw new FunctionalException(EXCEPTION_PARSE_JSON, e);
		}

		// On renseigne la date de dernière mise à jour
		movieBean.setLastUpdate(new Date());

	}

}
