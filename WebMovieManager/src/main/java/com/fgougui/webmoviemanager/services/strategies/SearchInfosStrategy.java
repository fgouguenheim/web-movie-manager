/**
 * 
 */
package com.fgougui.webmoviemanager.services.strategies;

import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.exceptions.FunctionalException;

import java.io.IOException;
import java.util.Set;

/**
 * Stratégie de récupération des infos
 */
public interface SearchInfosStrategy {

	/**
	 * Initialisation de l'API
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception;

	/**
	 * Effectue une recherche de film à partir du titre
	 * 
	 * @param movieName
	 * Nom du film
	 * @return resultats
	 * @throws FunctionalException
	 */
	public Set<MovieBean> findMovie(String movieName) throws FunctionalException, IOException;

	/**
	 * Remplit les informations sur le film
	 * 
	 * @param movieManagerBean
	 * Filmotheque
	 * @param String
	 * Url du fichier à renseigner
	 * @param idMovie
	 * Identifiant du film
	 * @return MovieBean Le film ajouté
	 * @throws FunctionalException
	 * @throws IOException
	 * @throws DataException
	 */
	public MovieBean fillMovie(final MovieManagerBean movieManagerBean, final String urlFileToFill, final long idMovie,
			final boolean toSee, final boolean alreadySeen) throws FunctionalException, IOException, DataException;

	/**
	 * Met à jour les critiques sur le film et la jaquette
	 * 
	 * @param movieBean
	 * MovieBean
	 * @throws FunctionalException
	 * @throws IOException
	 * @throws DataException
	 */
	public void updateMovie(final MovieBean movieBean) throws FunctionalException, IOException, DataException;
}
