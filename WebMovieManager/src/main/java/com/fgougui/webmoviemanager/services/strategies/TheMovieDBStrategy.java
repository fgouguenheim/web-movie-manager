/**
 * 
 */
package com.fgougui.webmoviemanager.services.strategies;

import com.fgougui.webmoviemanager.beans.ActorBean;
import com.fgougui.webmoviemanager.beans.MovieBean;
import com.fgougui.webmoviemanager.beans.MovieBean.Provider;
import com.fgougui.webmoviemanager.beans.MovieManagerBean;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.exceptions.FunctionalException;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.model.*;
import com.omertron.themoviedbapi.results.TmdbResultsList;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.EXCEPTION_ID_NOT_INTEGER;
import static com.fgougui.webmoviemanager.exceptions.ExceptionMessages.EXCEPTION_THEMOVIEDB;

/**
 * Stratégie de récupération des infos AlloCine
 */
@Component
public class TheMovieDBStrategy implements SearchInfosStrategy {

	/** Clé pour l'API */
	private static final String API_KEY = "5a1a77e2eba8984804586122754f969f";

	/** Préfixe de l'URL de YouTube pour les bandes annonces */
	private static final String YOUTUBE_PREFIX_URL = "https://www.youtube.com/watch?v=";

	/** Langue de recherche */
	private static final String FR = "fr";

	/** TheMovieDB API */
	private TheMovieDbApi tmdb;

	/** TheMovieDB Configuration */
	private TmdbConfiguration config;

	@Override
	public void init() throws MovieDbException {
		// Initialise l'API
		tmdb = new TheMovieDbApi(API_KEY);
		// Récupère la configuration
		config = tmdb.getConfiguration();
	}

	@Override
	public Set<MovieBean> findMovie(final String movieName) throws FunctionalException, IOException {

		final Set<MovieBean> movies = new LinkedHashSet<MovieBean>();

		try {
			// Construction de l'URL et lancement de la requête
			final TmdbResultsList<MovieDb> movieList = tmdb.searchMovie(movieName, 0, FR, false, 0);
			for (final MovieDb movieDb : movieList.getResults()) {
				final MovieBean movieBean = new MovieBean();

				// Identifiant du film
				movieBean.setId(Long.valueOf(movieDb.getId()));

				// Titre
				movieBean.setTitle(movieDb.getTitle());
				movieBean.setTitleOriginal(movieDb.getOriginalTitle());

				// Année de production
				try {
					movieBean.setYear(movieDb.getReleaseDate().substring(0, 4));
				} catch (final Exception e) {
					// Pas possible de déterminer l'année...
				}

				// URL de l'affiche
				try {
					setUrlPoster(movieBean, movieDb);
				} catch (final Exception e) {
					// Pas de poster...
				}

				// Ajout du film
				movies.add(movieBean);
			}
		} catch (final MovieDbException e) {
			throw new FunctionalException(EXCEPTION_THEMOVIEDB, e);
		}
		return movies;
	}

	@Override
	public MovieBean fillMovie(final MovieManagerBean movieManagerBean, final String urlFileToFill, final long id, final boolean toSee,
			final boolean alreadySeen) throws FunctionalException, IOException, DataException {

		// Le film à remplir
		final MovieBean movieBean = new MovieBean();
		movieBean.setId(id);
		movieBean.setUrlFile(urlFileToFill);
		movieBean.setToSee(toSee);
		movieBean.setAlreadySeen(alreadySeen);
		movieBean.setProvider(Provider.THEMOVIEDB);

		try {
			// Construction de l'URL et lancement de la requête
			final MovieDb movieDb = tmdb.getMovieInfo(Integer.parseInt(Long.toString(id)), FR,
					"alternative_titles,casts,images,keywords,releases,trailers,translations,similar_movies,reviews,lists");
			movieBean.setTitle(movieDb.getTitle());
			movieBean.setTitleOriginal(movieDb.getOriginalTitle());

			// Année de production
			try {
				movieBean.setYear(movieDb.getReleaseDate().substring(0, 4));
			} catch (final Exception e) {
				// Pas possible de déterminer l'année...
			}

			// Nationalités
			try {
				final List<String> nationalities = new ArrayList<String>();
				for (final ProductionCountry country : movieDb.getProductionCountries()) {
					final String nationality = country.getIsoCode();
					nationalities.add(nationality);
				}
				movieBean.setNationalities(nationalities);
			} catch (final Exception e) {
				// Pas de nationalités...
			}

			// Genres
			try {
				final List<String> genres = new ArrayList<String>();
				for (final Genre genreDb : movieDb.getGenres()) {
					final String genre = genreDb.getName();
					genres.add(genre);
				}
				movieBean.setGenres(genres);
			} catch (final Exception e) {
				// Pas de genres...
			}

			// Synopsis
			try {
				final String synopsis = movieDb.getOverview();
				movieBean.setSynopsis(synopsis);
			} catch (final Exception e) {
				// Pas de synopsis...
			}

			// Casting & réalisateur
			final List<ActorBean> casting = new ArrayList<ActorBean>();
			String director = null;
			try {
				for (final PersonCast personCast : movieDb.getCast()) {
					final String person = personCast.getName();
					String role = null;
					try {
						role = personCast.getCharacter();
					} catch (final Exception e) {
						// Pas de role...
					}
					final ActorBean acteur = new ActorBean(person, role);
					casting.add(acteur);
				}
			} catch (final Exception e) {
				// Pas de casting...
			}
			try {
				for (final PersonCrew personCrew : movieDb.getCrew()) {
					if (StringUtils.equalsIgnoreCase(personCrew.getJob(), "director")) {
						director = personCrew.getName();
						break;
					}
				}
			} catch (final Exception e) {
				// Pas de réalisateur...
			}
			movieBean.setCasting(casting);
			movieBean.setDirector(director);

			// Informations complémentaires
			// Pas d'informations complémentaires...

			// URL de l'affiche
			try {
				setUrlPoster(movieBean, movieDb);
			} catch (final Exception e) {
				// Pas de poster...
			}

			// URL de la bande annonce
			try {
				final Trailer youtubeTrailer = getYoutubeTrailer(movieDb);
				if (youtubeTrailer != null) {
					final String urlTrailer = YOUTUBE_PREFIX_URL + youtubeTrailer.getSource();
					movieBean.setUrlTrailer(urlTrailer);
				}
			} catch (final Exception e) {
				// Pas de bande annonce...
			}

			// Notes spectateurs
			try {
				// On divise par 2 car sur TheMovieDb les notes sont sur 10
				// et nous les voulons sur 5
				final float userRating = movieDb.getVoteAverage() / 2;
				movieBean.setUserRating(userRating);
			} catch (final Exception e) {
				// Pas de notes...
			}

		} catch (final NumberFormatException e) {
			throw new FunctionalException(EXCEPTION_ID_NOT_INTEGER, e);
		} catch (final MovieDbException e) {
			throw new FunctionalException(EXCEPTION_THEMOVIEDB, e);
		}
		return movieBean;
	}

	/**
	 * @param movieDb
	 * @return
	 */
	private Trailer getYoutubeTrailer(final MovieDb movieDb) {
		final List<Trailer> youtubeTrailers = new ArrayList<Trailer>();
		final List<Trailer> allTrailers = movieDb.getTrailers();
		for (final Trailer trailer : allTrailers) {
			if (trailer.getWebsite().equals(Trailer.WEBSITE_YOUTUBE)) {
				youtubeTrailers.add(trailer);
			}
		}
		if (!youtubeTrailers.isEmpty()) {
			return youtubeTrailers.get(0);
		}
		return null;
	}

	@Override
	public void updateMovie(final MovieBean movieBean) throws FunctionalException, IOException, DataException {
		// TODO : A implémenter
		return;
	}

	/**
	 * Construit l'URL du poster pour un film donné
	 * 
	 * @param movie
	 * @param movieDb
	 */
	private void setUrlPoster(final MovieBean movie, final MovieDb movieDb) {
		// Récupération de la taille originale (ou la + grande, càd la dernière de la liste)
		final List<String> sizes = config.getPosterSizes();
		String sizeToDownload = null;
		for (final Iterator<String> iterator = sizes.iterator(); iterator.hasNext();) {
			final String size = iterator.next();
			if (StringUtils.equalsIgnoreCase(size, "original") || !iterator.hasNext()) {
				sizeToDownload = size;
				break;
			}
		}
		// Remplissage de l'URL du poster
		if (StringUtils.isNotBlank(movieDb.getPosterPath())) {
			movie.setUrlPoster(config.getBaseUrl() + sizeToDownload + movieDb.getPosterPath());
			movie.setPoster(true);
		} else {
			movie.setPoster(false);
		}
	}

}
