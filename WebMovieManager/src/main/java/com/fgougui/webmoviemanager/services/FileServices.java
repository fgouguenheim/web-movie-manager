package com.fgougui.webmoviemanager.services;

import com.fgougui.webmoviemanager.exceptions.FunctionalException;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * Services sur les fichiers disque
 */
@Service
public class FileServices {

	/**
	 * Supprime un fichier du disque
	 * 
	 * @param urlFile URL du fichier
	 * @throws FunctionalException
	 */
	public void deleteFile(final String urlFile) throws FunctionalException {
		final File file = new File(urlFile);
		if (file.exists() && file.isFile()) {
			final boolean isDeleted = file.delete();
			if (!isDeleted) {
				throw new FunctionalException("Impossible d'effacer le fichier.");
			}
		}
	}

}
