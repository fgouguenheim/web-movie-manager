package com.fgougui.webmoviemanager.config;

import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Properties;

/**
 * Classe pour les propriétées relatives au stockage des données sur le disque
 */
public class ConfigProperties {

	/** Dossier contenant les posters originaux */
	private static final String FOLDER_POSTERS_ORIGINAL = "originalPosters";

	/** Dossier contenant les posters redimensionnés */
	private static final String FOLDER_POSTERS_RESIZED = "thumbnailPosters";

	/** Instance */
	private static ConfigProperties instance = null;

	/** Stockage des propriétés de configuration */
	private Properties config;

	/**
	 * Renvoie le singleton
	 * 
	 * @return ConfigProperties
	 * @throws DataException
	 */
	public static synchronized ConfigProperties getInstance() throws DataException {
		if (instance == null) {
			instance = new ConfigProperties();
		}
		return instance;
	}

	/**
	 * Constructeur privé
	 * 
	 * @throws DataException
	 */
	private ConfigProperties() throws DataException {
		readProperties();
	}

	/**
	 * Lit et parse le fichier de propriétés
	 * 
	 * @throws DataException
	 */
	private void readProperties() throws DataException {
		final URL configFileUrl = ConfigProperties.class.getResource("config.properties");
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(configFileUrl.toURI()));
			final Properties configLocal = new Properties();
			configLocal.load(fis);
			this.config = configLocal;
		} catch (final Exception e) {
			config = new Properties();
			throw new DataException("Impossible de lire le fichier de configuration.", e);
		} finally {
			IOUtils.closeQuietly(fis);
		}
	}

	/**
	 * Ecrit le fichier de propriétés
	 * 
	 * @throws DataException
	 */
	public final void writeProperties() throws DataException {
		final URL configFileUrl = ConfigProperties.class.getResource("config.properties");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(configFileUrl.toURI()));
			config.store(fos, "Configuration de l'application");
		} catch (final Exception e) {
			throw new DataException("Impossible d'écrire le fichier de configuration.", e);
		} finally {
			IOUtils.closeQuietly(fos);
		}
	}

	/**
	 * Si la chaine passée en caractère est nulle, lance une exception de
	 * configuration
	 * 
	 * @throws ConfigurationException
	 */
	private String blankToConfigurationException(final String result) throws ConfigurationException {
		if (StringUtils.isBlank(result)) {
			throw new ConfigurationException();
		}
		return result;
	}

	/**
	 * Concatène un tableau de String avec une virgule et un espace
	 * 
	 * @param value
	 * String[]
	 * @return String
	 */
	@SuppressWarnings("unused")
	private String separateWithComma(final String[] value) {
		final StringBuilder buffer = new StringBuilder();
		if (value != null) {
			for (int i = 0; i < value.length; i++) {
				final String string = value[i];
				buffer.append(string);
				if (i != value.length) {
					buffer.append(", ");
				}
			}
		}
		return buffer.toString();
	}

	/**
	 * Renvoie un tableau de String à partir d'une String contenant plusieurs
	 * valeurs séparées par une virgule
	 * 
	 * @param configurationCommaSeparated
	 * String
	 * @return String[]
	 */
	public String[] splitAndTrim(final String configurationCommaSeparated) {
		final String[] result = StringUtils.split(configurationCommaSeparated, ",");
		for (int i = 0; i < result.length; i++) {
			result[i] = result[i].trim();
		}
		return result;
	}

	/**
	 * Renvoie le séparateur pour le système de fichier
	 * 
	 * @return String
	 */
	public final String getFileSystemSeparator() {
		// On regarde si le nom donné en paramètre contient des / ou des \
		try {
			if (StringUtils.contains(getDataFilesPath(), "/") && !StringUtils.contains(getDataFilesPath(), "\\")) {
				return "/";
			} else if (!StringUtils.contains(getDataFilesPath(), "/") && StringUtils.contains(getDataFilesPath(), "\\")) {
				return "\\";
			}
		} catch (final ConfigurationException e) {
		}
		// Sinon par défaut on renvoie le séparateur système
		return System.getProperty("file.separator");
	}

	// GETTERS/SETTERS UTILITAIRES

	public final String getPostersOriginalFilesPath() throws ConfigurationException {
		return getDataFilesPath() + FOLDER_POSTERS_ORIGINAL + getFileSystemSeparator();
	}

	public final String getPostersThumbnailFilesPath() throws ConfigurationException {
		return getDataFilesPath() + FOLDER_POSTERS_RESIZED + getFileSystemSeparator();
	}

	// GETTERS/SETTERS

	/**
	 * Renvoie les chemins des répertoires contenant les films
	 * 
	 * @return String[]
	 * @throws ConfigurationException
	 */
	public final String[] getMovieManagerFilesPath() throws ConfigurationException {
		final String movieManagerFilesPathProperty = blankToConfigurationException(config.getProperty("moviemanager.files.path"));
		return splitAndTrim(movieManagerFilesPathProperty);
	}

	/**
	 * Renvoie les chemins des répertoires contenant les films séparés par une
	 * virgule
	 * 
	 * @return String
	 * @throws ConfigurationException
	 */
	public final String getMovieManagerFilesPathCommaSeparated() throws ConfigurationException {
		final String movieManagerFilesPathProperty = blankToConfigurationException(config.getProperty("moviemanager.files.path"));
		return movieManagerFilesPathProperty;
	}

	/**
	 * Modifie les chemins des répertoires contenant les films
	 * 
	 * @param movieManagerFilesPath
	 * Liste des répertoires séparés par une virgule
	 */
	public final void setMovieManagerFilesPathCommaSeparated(final String movieManagerFilesPath) {
		config.setProperty("moviemanager.files.path", movieManagerFilesPath);
	}

	/**
	 * Renvoie les extensions à ignorer
	 * 
	 * @return String[]
	 * @throws ConfigurationException
	 */
	public final String[] getMovieManagerFilesExtensionIgnore() throws ConfigurationException {
		final String movieManagerFilesExtensionIgnoreProperty = blankToConfigurationException(config
				.getProperty("moviemanager.files.extension.ignore"));
		return splitAndTrim(movieManagerFilesExtensionIgnoreProperty);
	}

	/**
	 * Renvoie les extensions à ignorer séparées par une virgule
	 * 
	 * @return String
	 * @throws ConfigurationException
	 */
	public final String getMovieManagerFilesExtensionIgnoreCommaSeparated() throws ConfigurationException {
		final String movieManagerFilesExtensionIgnoreProperty = blankToConfigurationException(config
				.getProperty("moviemanager.files.extension.ignore"));
		return movieManagerFilesExtensionIgnoreProperty;
	}

	/**
	 * Modifie les extensions à ignorer
	 * 
	 * @param movieManagerFilesExtensionIgnore
	 * Liste des extensions séparées par une virgule
	 */
	public final void setMovieManagerFilesExtensionIgnoreCommaSeparated(final String movieManagerFilesExtensionIgnore) {
		config.setProperty("moviemanager.files.extension.ignore", movieManagerFilesExtensionIgnore);
	}

	/**
	 * Indique si le renommage automatique est activé
	 * 
	 * @return Boolean
	 */
	public final Boolean isMovieManagerAutoRename() {
		return Boolean.parseBoolean(config.getProperty("moviemanager.auto.rename"));
	}

	/**
	 * Modifie l'activation du renommage automatique
	 * 
	 * @param movieManagerAutoRename
	 */
	public final void setMovieManagerAutoRename(final Boolean movieManagerAutoRename) {
		config.setProperty("moviemanager.auto.rename", movieManagerAutoRename.toString());
	}

	/**
	 * Renvoie le chemin de stockage des fichiers
	 * 
	 * @return String
	 * @throws ConfigurationException
	 */
	public final String getDataFilesPath() throws ConfigurationException {
		return blankToConfigurationException(config.getProperty("data.files.path"));
	}

	/**
	 * Modifie le chemin de stockage des fichiers
	 * 
	 * @param dataFilesPath
	 */
	public final void setDataFilesPath(final String dataFilesPath) {
		config.setProperty("data.files.path", dataFilesPath);
	}

	/**
	 * Indique si le mode sauvegarde est activé
	 * 
	 * @return Boolean
	 */
	public final Boolean isDataBackup() {
		return Boolean.parseBoolean(config.getProperty("data.is.backup"));
	}

	/**
	 * Modifie l'activation du mode sauvegarde
	 * 
	 * @param dataBackup
	 */
	public final void setDataBackup(final Boolean dataBackup) {
		config.setProperty("data.is.backup", dataBackup.toString());
	}

	/**
	 * Indique si le serveur est derrière un proxy
	 * 
	 * @return Boolean
	 */
	public final Boolean isNetworkProxy() {
		return Boolean.parseBoolean(config.getProperty("network.isproxy"));
	}

	/**
	 * Modifie l'activation du proxy
	 * 
	 * @param networkProxy
	 */
	public final void setNetworkProxy(final Boolean networkProxy) {
		config.setProperty("network.isproxy", networkProxy.toString());
	}

	/**
	 * Renvoie l'hote du proxy
	 * 
	 * @return String
	 */
	public final String getNetworkProxyHost() {
		return StringUtils.defaultString(config.getProperty("network.proxy.host"));
	}

	/**
	 * Modifie l'hote du proxy
	 * 
	 * @param networkProxyHost
	 */
	public final void setNetworkProxyHost(final String networkProxyHost) {
		config.setProperty("network.proxy.host", networkProxyHost);
	}

	/**
	 * Renvoie le port du proxy
	 * 
	 * @return String
	 */
	public final String getNetworkProxyPort() {
		return StringUtils.defaultString(config.getProperty("network.proxy.port"));
	}

	/**
	 * Modifie le port du proxy
	 * 
	 * @param networkProxyPort
	 */
	public final void setNetworkProxyPort(final String networkProxyPort) {
		config.setProperty("network.proxy.port", networkProxyPort);
	}

	/**
	 * Renvoie le login d'authentification sur le proxy
	 * 
	 * @return String
	 */
	public final String getNetworkProxyLogin() {
		return StringUtils.defaultString(config.getProperty("network.proxy.login"));
	}

	/**
	 * Modifie le login d'authentification sur le proxy
	 * 
	 * @param networkProxyLogin
	 */
	public final void setNetworkProxyLogin(final String networkProxyLogin) {
		config.setProperty("network.proxy.login", networkProxyLogin);
	}

	/**
	 * Renvoie le mot de passe d'authentification sur le proxy
	 * 
	 * @return String
	 */
	public final String getNetworkProxyPassword() {
		return StringUtils.defaultString(config.getProperty("network.proxy.password"));
	}

	/**
	 * Modifie le mot de passe d'authentification sur le proxy
	 * 
	 * @param networkProxyPassword
	 */
	public final void setNetworkProxyPassword(final String networkProxyPassword) {
		config.setProperty("network.proxy.password", networkProxyPassword);
	}
}
