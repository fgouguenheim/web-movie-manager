import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.fgougui.webmoviemanager.config.ConfigProperties;
import com.fgougui.webmoviemanager.exceptions.ConfigurationException;
import com.fgougui.webmoviemanager.exceptions.DataException;
import com.fgougui.webmoviemanager.utils.GraphicsUtils;


public class BatchThumbnails {

	public static void main(String[] args) throws IOException, ConfigurationException, DataException {
		String postersOriginalFilesPath = ConfigProperties.getInstance().getPostersOriginalFilesPath();
		File originalPostersFolder = new File(postersOriginalFilesPath);
		String[] originalPostersFilesNames = originalPostersFolder.list();
		for (String originalPosterFileName : originalPostersFilesNames) {
			String originalPosterFilePath = postersOriginalFilesPath + originalPosterFileName;
			if (originalPosterFilePath.endsWith(".jpg")) {
				String outputPicturePath = StringUtils.replace(originalPosterFilePath, "originalPosters", "thumbnailPosters");
				GraphicsUtils.createJPGThumbnail(originalPosterFilePath, outputPicturePath);
			}
		}
	}
	
}
