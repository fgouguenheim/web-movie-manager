/**
 * Convertit du texte vers le format HTML
 * Utilisé pour remplir un <input> à partir d'un <textarea>
 * @param s String
 * @return String
 */
function convertTextToHTML(s) {
	s = s.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\n/g, "<br>");//.replace(/  /g, "&nbsp; ");
	return s;
}

/**
 * Convertit du texte depuis le format HTML
 * Utilisé pour remplir un <textarea> à partir d'un <input>
 * @param s String
 * @return String
 */
function convertHTMLToText(s) {
	s = s.replace(/\&amp;/g,"&").replace(/\&lt;/g,"<").replace(/&gt;/g,">").replace(/<br>/g,"\n");//.replace(/\&nbsp;/g, " ");
	return s;
}

/**
 * Convertit du texte vers le format ASCII standard URL
 * Utilisé pour envoyer un texte au serveur
 * @param s
 * @return
 */
function convertTextToURL(s) {
//s = s.replace(/€/g,"%80");
	//Encodage de l'URL
	s = escape(s);
	//Remplacement du + (et du /)
	s = s.replace(/\+/g,"%2B");
	//Remplacement du code unicode pour l'euro
	s = s.replace(/%u20AC/g,"%80");
	return s;
}

/**
 * Taille automatique d'une textarea
 * @param editeur HTML Element
 */
function dynamicHeight(editeur) {
	//Taille dynamique de l'éditeur
	hauteurLigne = editeur.style.height;
	var rows = editeur.value.split('\n');
	nRows=-1;
	for (i=0;i < rows.length; i++) {
		if (rows[i].length >= editeur.cols) 
		{
			nRows+= Math.floor(rows[i].length/editeur.cols);
		}
	}
	nRows += rows.length;
	if (nRows == 1) {
		editeur.rows = 1;
	} else if (nRows == 0) {
		editeur.rows = 1;
		editeur.style.height = hauteurLigne;
	} else {
		editeur.rows = nRows;
		editeur.style.height = "auto";
	}
}

/**
 * Fonction de formattage de la date
 * Précondition : la date a déjà été vérifiée par le serveur
 * @param date Date
 */
function formatDate(date) {
	if (date == "") {
		return "";
	}
	var temp = date.split('/');
	var j = temp[0]; // jour
    var m = temp[1]; // mois
    var a = temp[2]; // année

    // Ajout de '0' au jour et au mois si besoin
    if (j.length == 1)
    	j = '0' + j;

    if (m.length == 1)
    	m = '0' + m;

    //Si l'année est entre 00 et 99, on ajoute 2000 (ex: on peut écrire 03 pour 2003)
    if (a.length == 1)
    	a = '200' + a
    if (a.length == 2)
    	a = '20' + a
    
    return j + "/" + m + "/" + a;
}

/**
 * Compare deux dates
 * @param dateA
 * @param dateB
 * @param desc
 */
function compareDate(dateA, dateB, desc) {
    //Prise en compte du sens de tri
    res = 1;
    if(desc) { res = -1 }

    //Gestion des dates vides
    if(dateA == "") {
        return (dateB == "") ? 0 : -res;
    } else if(dateB == "") {
        return res;
    }

    //Décomposition de la date
    Aa = dateA.substring(6,10);
    Am = dateA.substring(3,5);
    Aj = dateA.substring(0,2);
    Ba = dateB.substring(6,10);
    Bm = dateB.substring(3,5);
    Bj = dateB.substring(0,2);

    //Comparaison
    if (Aa > Ba) { return res; }
    else if (Aa < Ba) { return -res; }
    else if (Am > Bm) { return res; }
    else if (Am < Bm) { return -res; }
    else if (Aj > Bj) { return res; }
    else if (Aj < Bj) { return -res; }
    else { return 0; }
}