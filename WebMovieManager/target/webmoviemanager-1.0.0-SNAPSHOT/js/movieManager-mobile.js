//Chemin de JQTouch
var pathJQTouch = './js/jquery/jqtouch/';
//Initialisation
var jQT = new $.jQTouch({
	//Icone sur l'écran d'accueil
    icon: '../images/mobile/springboard_icon.png',
    //Effet brillant sur l'icone
    addGlossToIcon: false,
    //Ecran de démarrage
    startupScreen: '../images/mobile/startup_screen.png',
    //Style de la barre de statut lorsque en plein écran
    statusBar: 'black',
    //Images préchargées
    preloadImages: [
        pathJQTouch + 'themes/jqt/img/back_button.png',
        pathJQTouch + 'themes/jqt/img/back_button_clicked.png',
        pathJQTouch + 'themes/jqt/img/button_clicked.png',
        pathJQTouch + 'themes/jqt/img/grayButton.png',
        pathJQTouch + 'themes/jqt/img/whiteButton.png',
        pathJQTouch + 'themes/jqt/img/loading.gif'
        ]
});

//Au démarrage, 
$(document).ready(function(){
	//Réglage des paramètres par défaut des requêtes AJAX
	$.ajaxSetup({
		timeout: 100000
	});
	
    // Cet évènement nous permets d'effacer le contenu du div lors du retour
    $('#listMovies').bind('pageAnimationEnd', function(e, info){
         if (!$('#listMovies').data('loaded')) {
               $('#listMoviesAjax').html('Chargement...');
          }
    });
});

/**
 * Filtre les films par genre
 * @param genre
 */
function filterByGenre(genre){
	var urlFiltre = URL_MOVIE_MANAGER_SERVLET + '?action=execFilter';
	if (genre != 'all') {
		genre = convertTextToURL(genre);
		genre = genre.replace(/_/g,"%20");
		urlFiltre = urlFiltre + '&genre=' + genre;
	}
    $('#listMovies').data('loaded', false);
    if (!$('#listMovies').data('loaded')) {
             $('#listMoviesAjax').load(urlFiltre);
             $('#listMovies').data('loaded', true);
             jQT.goTo('#listMovies','slide'); //on simule le click pour afficher la liste des films
    }
	//Le bouton de retour devient "genres"
	$('#btn_retour_listMovies').html("Genres");
	//Le titre de la page devient le genre
	$('#titleListMovies').html(unescape(genre));
}


/**
 * Filtre les films par dossier
 * @param folder
 */
function filterByFolder(folder) {
	var urlFilter = URL_MOVIE_MANAGER_SERVLET + '?action=execFilter';
	if (folder != 'all') {
		folder = convertTextToURL(folder);
		folder = folder.replace(/_/g,"%20");
		urlFilter = urlFilter + '&folder=' + folder;
	}
    $('#listMovies').data('loaded', false);
    if (!$('#listMovies').data('loaded')) {
             $('#listMoviesAjax').load(urlFilter);
             $('#listMovies').data('loaded', true);
             jQT.goTo('#listMovies','slide'); //on simule le click pour afficher la liste des films
    }
	//Le bouton de retour devient "folder"
	$('#btn_retour_listMovies').html("Dossiers");
	//Le titre de la page devient le dossier
	$('#titleListMovies').html(unescape(folder));
}

/**
 * Affiche les infos d'un film
 * @param idMovie
 */
function loadMovieInfos(idMovie) {
	var urlInfos = URL_MOVIE_SERVLET + '?action=execDisplayInfos&idMovie=' + idMovie;
    $('#movieInfos').data('loaded', false);
    if (!$('#movieInfos').data('loaded')) {
             $('#movieInfosAjax').load(urlInfos, function(html) {
            	//On affiche les notes sous forme graphique
     			loadRatings($('#pressRatingValue').val(), $('#userRatingValue').val());
            	 //Si la dernière mise à jour date de plus de 10 jours, alors on met à jour les infos
            	 var nbDaysSinceLastUpdate = $('#nbDaysSinceLastUpdate').val();
            	 if (nbDaysSinceLastUpdate > 10) {
                	 updateMovieInfos(idMovie);
                 }
             });
             $('#movieInfos').data('loaded', true);
             jQT.goTo('#movieInfos','slide'); //on simule le click pour afficher les infos du film
    }
}

/**
 * Met à jour les infos d'un film depuis AlloCine
 * @param idMovie
 */
function updateMovieInfos(idMovie) {
	//On lance la requête
	$.getJSON(
		URL_MOVIE_SERVLET + '?action=execUpdateInfos',
		{idMovie: idMovie},
		function(jsonObject) {
			//On met à jour les critiques
			loadRatings(jsonObject.pressRating, jsonObject.userRating);
		}
	);
}

/**
 * Affiche les notes sous forme graphique
 * @param pressRating
 * @param userRating
 */
function loadRatings(pressRating, userRating) {
	var pressRatingValue = new Number(pressRating);
	var userRatingValue = new Number(userRating);
	// Efface les étoiles éventuellement déjà présentes
	$('#pressRatingContainer').removeClass("rating0").removeClass("rating05").removeClass("rating1").removeClass("rating15").removeClass("rating2").removeClass("rating25").removeClass("rating3").removeClass("rating35").removeClass("rating4").removeClass("rating45").removeClass("rating5");
	$('#userRating').removeClass("rating0").removeClass("rating05").removeClass("rating1").removeClass("rating15").removeClass("rating2").removeClass("rating25").removeClass("rating3").removeClass("rating35").removeClass("rating4").removeClass("rating45").removeClass("rating5");
	// Affiche ou non la zone
	if (pressRatingValue == 0) {
		$('#pressRating').hide();
	} else {
		$('#pressRating').show();
	}
	if (userRatingValue == 0) {
		$('#userRating').hide();
	} else {
		$('#userRating').show();
	}
	// Charge les étoiles
	if (pressRatingValue == 0) {
		$('#pressRatingContainer').addClass("rating0");
	} else if (pressRatingValue <= 0.5) {
		$('#pressRatingContainer').addClass("rating05");
	} else if (pressRatingValue <= 1) {
		$('#pressRatingContainer').addClass("rating1");
	} else if (pressRatingValue <= 1.5) {
		$('#pressRatingContainer').addClass("rating15");
	} else if (pressRatingValue <= 2) {
		$('#pressRatingContainer').addClass("rating2");
	} else if (pressRatingValue <= 2.5) {
		$('#pressRatingContainer').addClass("rating25");
	} else if (pressRatingValue <= 3) {
		$('#pressRatingContainer').addClass("rating3");
	} else if (pressRatingValue <= 3.5) {
		$('#pressRatingContainer').addClass("rating35");
	} else if (pressRatingValue <= 4) {
		$('#pressRatingContainer').addClass("rating4");
	} else if (pressRatingValue <= 4.5) {
		$('#pressRatingContainer').addClass("rating45");
	} else {
		$('#pressRatingContainer').addClass("rating5");
	}
	if (userRatingValue == 0) {
		$('#userRatingContainer').addClass("rating0");
	} else if (userRatingValue <= 0.5) {
		$('#userRatingContainer').addClass("rating05");
	} else if (userRatingValue <= 1) {
		$('#userRatingContainer').addClass("rating1");
	} else if (userRatingValue <= 1.5) {
		$('#userRatingContainer').addClass("rating15");
	} else if (userRatingValue <= 2) {
		$('#userRatingContainer').addClass("rating2");
	} else if (userRatingValue <= 2.5) {
		$('#userRatingContainer').addClass("rating25");
	} else if (userRatingValue <= 3) {
		$('#userRatingContainer').addClass("rating3");
	} else if (userRatingValue <= 3.5) {
		$('#userRatingContainer').addClass("rating35");
	} else if (userRatingValue <= 4) {
		$('#userRatingContainer').addClass("rating4");
	} else if (userRatingValue <= 4.5) {
		$('#userRatingContainer').addClass("rating4.5");
	} else {
		$('#userRatingContainer').addClass("rating5");
	}
}

/**
 * Recherche un film
 */
function searchMovies() {
	var pattern = $('#searchField').val();
	var urlSearch = URL_MOVIE_MANAGER_SERVLET + '?action=execSearchMovies&q=' + pattern;
    $('#listMoviesSearch').data('loaded', false);
    if (!$('#listMoviesSearch').data('loaded')) {
             $('#listMoviesSearchAjax').load(urlSearch);
             $('#listMoviesSearch').data('loaded', true);
             jQT.goTo('#listMoviesSearch','slide'); //on simule le click pour afficher la liste des films
    }
}

/**
 * Supprime un film de la filmotheque
 */
function deleteFromMovieManager() {
	//Demande de confirmation
	if (confirm("Ceci supprimera le film de la filmothèque. Etes-vous sûr de vouloir faire ceci ?")) {
		//On récupère l'url du fichier
		var idMovie = $('#idMovie').val();
		//On lance la requête
		$.post(
			URL_MOVIE_SERVLET + '?action=execDeleteFromMovieManager',
			{idMovie: idMovie},
			function(html) {
				jQT.goTo('#home','dissolve');
			}
		);
	}
}

/**
 * Supprime un film du disque
 */
function deleteFromDisk() {
	if (confirm("Ceci supprimera le film du disque. Etes-vous sûr de vouloir faire ceci ?")) {
		if (confirm("ATTENTION : CETTE ACTION EST IRREVERSIBLE. Etes-vous sûr de vouloir faire ceci ?")) {
			//On récupère l'url du fichier
			var idMovie = $('#idMovie').val();
			//On lance la requête
			$.post(
				URL_MOVIE_SERVLET + '?action=execDeleteFromDisk',
				{idMovie: idMovie},
				function(html) {
					jQT.goTo('#home','dissolve');
				}
			);
		}
	}
}