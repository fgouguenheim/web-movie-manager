<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:useBean id="movieManager" class="com.fgougui.webmoviemanager.beans.MovieManagerBean" scope="application"/>
<%@ page import="com.fgougui.webmoviemanager.beans.MovieBean" %>

  
<!-- Liste des films -->
<ul class="rounded">
	<%
		if (request.getParameter("folder") != null) {
			for (MovieBean movie : movieManager.getMoviesByFolder(request.getParameter("folder"))) {
	%>
			<li id="movie_<%= movie.getId() %>" class="arrow movie <% for(String genre : movie.getGenres()) { %> genre_<%= genre.replaceAll(" ", "_") %><% } %> folder_<%=movie.getFolder().replaceAll(" ", "_")%>"
				onclick="loadMovieInfos(<%=movie.getId()%>)">
				<span><%=movie.getTitle()%></span>
			</li>
		<%
			}
			} else if (request.getParameter("genre") != null) {
				for (MovieBean movie : movieManager.getMoviesByGenre(request.getParameter("genre"))) {
		%>
			<li id="movie_<%=movie.getId()%>" class="arrow movie <%for(String genre : movie.getGenres()) {%> genre_<%=genre.replaceAll(" ", "_")%><%}%> folder_<%=movie.getFolder().replaceAll(" ", "_")%>"
				onclick="loadMovieInfos(<%=movie.getId()%>)">
				<span><%=movie.getTitle()%></span>
			</li>
		<%
			}
			} else {
				for (MovieBean movie : movieManager.getMovies()) {
		%>
		<li id="movie_<%=movie.getId()%>" class="arrow movie <%for(String genre : movie.getGenres()) {%> genre_<%=genre.replaceAll(" ", "_")%><%}%> folder_<%=movie.getFolder().replaceAll(" ", "_")%>"
			onclick="loadMovieInfos(<%=movie.getId()%>)">
			<span><%=movie.getTitle()%></span>
		</li>
		<% }
	} %>
</ul>
