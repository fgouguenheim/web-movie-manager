<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<!-- JQuery -->
	<script type="text/javascript" src="/WebMovieManager/js/jquery/jquery-1.6.1.min.js"></script>
	
	<!-- JQuery UI -->
	<script type="text/javascript" src="/WebMovieManager/js/jquery/jquery-ui/js/jquery-ui-1.8.10.custom.min.js"></script>
	
	<!-- CSS de l'application -->
	<link rel="stylesheet" type="text/css" href="/WebMovieManager/css/computer/base.css">
	<link rel="stylesheet" type="text/css" href="/WebMovieManager/css/computer/index.css">
	<link rel="stylesheet" type="text/css" href="/WebMovieManager/css/computer/movieManager.css">
	
	<!-- CSS JQuery UI --> 
	<link rel="stylesheet" type="text/css" href="/WebMovieManager/js/jquery/jquery-ui/css/humanity/jquery-ui-1.8.10.custom.css">
	
	<!-- Script de la page -->
	<script type="text/javascript" src="/WebMovieManager/js/base.js"></script>
	<script type="text/javascript" src="/WebMovieManager/js/movieManager-computer.js"></script>
	
	<!-- Titre de l'application -->
	<title>WebMovieManager</title>

</head>

<body>

	<div id="header">
	
		<!-- Zone d'informations "Gmail like" -->
	    <div id="zoneInfo">
	        <div id="zoneInfoMsg">
	        </div>
	    </div>
	
		<!-- Titre de l'application -->
		<div id="title">
			<label>WebMovieManager</label>
		</div>
	
	</div>
	
	<!-- Menu -->
	<div id="menu">
	    <ul>
	    	<li><label class="menuLink" onclick="loadMovieManager()">Liste des films</label></li>
	    	<li><label class="menuLink" onclick="synchro()">Synchronisation</label></li>
	    	<li><label class="menuLink" onclick="loadConfiguration()">Configuration</label></li>
	    </ul>
	</div>
	
	<!-- Zone destinee au contenu la page -->
	<div id="content">
	</div>
	
	<!-- Informations sur les outils utilises -->
	<div id="infoLicence">
	    OpenSource UI Framework : <a href="http://jquery.com/">JQuery</a>.&nbsp;
	    Powered by : <a href="http://tomcat.apache.org/">Apache Tomcat</a>,&nbsp;<a href="http://www.eclipse.org/">Eclipse</a>.
	</div>

</body>
</html>