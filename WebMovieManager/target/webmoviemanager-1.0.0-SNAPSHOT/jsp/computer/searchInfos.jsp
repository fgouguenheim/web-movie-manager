<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.fgougui.webmoviemanager.beans.ActorBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.fgougui.webmoviemanager.beans.MovieBean"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>

<!-- Titre -->

<h2>R&eacute;sultats de la recherche pour le fichier "<%= request.getAttribute("fileName") %>"</h2>
<% if (request.getAttribute("nameMovieToSearch") != null) { %>
	<h3>(nom de film recherch&eacute; : "<%= request.getAttribute("nameMovieToSearch") %>")</h3>
<% } %>

<!-- Champs cachés -->
<input type="hidden" id="urlFile" value="<%= request.getAttribute("urlFile") %>"/>

<!-- Nb films restants à synchroniser -->
<h3><%= request.getAttribute("nbMoviesRemaining") %> films restants</h3>

<!-- Marquer comme à voir / déjà vu -->
<% if (((Set<MovieBean>) request.getAttribute("movies")).size() > 0) { %>
	<input type="button" id="toSee" value="A voir" class="button" onclick="$(this).toggleClass('selected');"/>
	<input type="button" id="alreadySeen" value="Déjà vu" class="button" onclick="$(this).toggleClass('selected');"/>
	<span class="buttonSeparator">&nbsp;</span>
<% } %>

<!-- Résultats de recherche -->
<input type="button" id="btnWatchMovie" value="Prévisualiser" class="button" onclick="watchMovie()"/>

<table id="searchInfosResults">
	<% for(MovieBean movie : (Set<MovieBean>) request.getAttribute("movies")) { %>
	<tr>
		<!-- Jaquette -->
		<td>
			<% if (movie.getUrlPoster() != null) { %>
				<img alt="poster" class="poster" src="<%= movie.getUrlPoster() %>">
			<% } else { %>
				<img alt="poster" src="./images/computer/nopicture.jpg">
			<% } %>
		</td>
		<td>
			<!-- Titre (& éventuel titre original) -->
			<%= movie.getTitle() %>
			<br/>
			<!-- Année de production -->
			<% if (StringUtils.isNotBlank(movie.getYear())) { %>
				<%= movie.getYear() %>
				<br/>
			<% } %>
			<!-- Réalisateur (éventuel) -->
			<% if (StringUtils.isNotBlank(movie.getDirector())) { %>
				de <%= movie.getDirector() %>
				<br/>
			<% } %>
			<!-- Casting (éventuel) -->
			<% if (movie.getCasting() != null && !(movie.getCasting().isEmpty())) { %>
				avec
				<% Iterator<ActorBean> it = movie.getCasting().iterator();
					while (it.hasNext()) { 
						ActorBean acteur = it.next(); %>
						<%=acteur.getName()%><% if (it.hasNext()) { %>,<% } %>
					<% } %>
			<% } %>
		</td>
		<!-- Bouton de sélection du résultat -->
		<td>
			<input type="button" value="Sélectionner ce résultat" class="button" onclick="selectMovieInfos(<%= movie.getId() %>)"/>
		</td>
	</tr>
	<% } %>
</table>

<!-- Bouton permettant de signaler que le fichier n'est pas un film -->
<input type="button" value="Ce fichier n'est pas un film" class="button" onclick="selectNotAMovie()"/>
<br/>
<!-- Formulaire permettant de modifier la recherche -->
<input type="button" value="Modifier le nom à rechercher" class="button" onclick="editSearchName()"/>
<br/>
<form>
	<input type="text" id="searchName" class="large" style="display: none;" value="<%= request.getAttribute("movieName") %>"/>
	<input type="button" id="btnSearchName" value="Relancer la recherche" class="button" style="display: none;" onclick="searchNewName()"/>
</form>
<!-- Formulaire permettant de renseigner l'id du film manuellement -->
<input type="button" value="Renseigner l'identifiant manuellement" class="button" onclick="enterIdMovieManually()"/>
<br/>
<form>
	<input type="text" id="idMovie" class="medium" style="display: none;"/>
	<input type="button" id="btnValidateId" value="Valider l'identifiant" class="button" style="display: none;" onclick="validateManualIdMovie()"/>
</form>