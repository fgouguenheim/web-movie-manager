<%@page import="com.fgougui.webmoviemanager.beans.MovieBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@page import="java.util.Set"%>
<%@page import="com.fgougui.webmoviemanager.beans.ActorBean"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.fgougui.webmoviemanager.utils.DateUtils"%>
<%@page import="java.util.Date"%>

<jsp:useBean id="movie" class="com.fgougui.webmoviemanager.beans.MovieBean" scope="request"/>

<!-- Titre (& éventuel titre original) -->
<h3><%=movie.getTitle()%></h3>

<!-- Champs cachés -->
<input type="hidden" id="urlFile" value="<%=movie.getUrlFile()%>"/>
<input type="hidden" id="urlTrailer" value="<%=movie.getUrlTrailer()%>"/>
<input type="hidden" id="idMovie" value="<%=movie.getId()%>"/>
<input type="hidden" id="nbDaysSinceLastUpdate" value="<%=DateUtils.daysBetween(movie.getLastUpdate(), new Date())%>"/>

<!-- Jaquette -->
<div class="poster">
	<%
		if (movie.getUrlPosterOriginal() != null) {
	%>
		<img alt="poster" src="/WebMovieManager/Auth/Movie?action=execDisplayPosterOriginal&idMovie=<%=movie.getId()%>" class="poster" id="posterMovieInfos_<%=movie.getId()%>">
	<%
		} else {
	%>
		<img alt="poster" src="./images/computer/nopicture.jpg" id="posterMovieInfos_<%=movie.getId()%>">
	<%
		}
	%>
</div>

<!-- Boutons -->
<div id="movieButtons">
	<!-- Bouton de visualisation -->
	<input type="button" id="btnWatchMovie" value="Visualiser" class="button" onclick="watchMovie()"/>
	<!-- Boutons de suppression -->
	<input type="button" id="btnDeleteFromMovieManager" value="Supprimer de la filmotheque" class="button" onclick="deleteFromMovieManager()"/>
	<input type="button" id="btnDeleteFromDisk" value="Supprimer du disque" class="button" onclick="deleteFromDisk()"/>
</div>

<!-- A voir ? -->
<input type="button" id="toSee_<%= movie.getId() %>" value="A voir" class="button <% if(movie.isToSee()) {%>selected<% } %>" onclick="setToSee(<%= movie.getId() %>)"/>
<input type="hidden" id="movieToSee_<%= movie.getId() %>" value="<%= movie.isToSee() %>"/>

<!--Déjà vu ? -->
<input type="button" id="alreadySeen_<%= movie.getId() %>" value="Déjà vu" class="button <% if(movie.isAlreadySeen()) {%>selected<% } %>" onclick="setAlreadySeen(<%= movie.getId() %>)"/>
<input type="hidden" id="movieAlreadySeen_<%= movie.getId() %>" value="<%= movie.isAlreadySeen() %>"/>

<!-- Infos du film -->
<div id="movieDetails">
	<!-- Réalisateur (éventuel) -->
	<div>
		<%
			if (StringUtils.isNotBlank(movie.getDirector())) {
		%>
			<b>de</b> <%=movie.getDirector()%>
		<%
			}
		%>
	</div>
	<!-- Année de production -->
	<div>
		<b>Produit en </b>
		<%= movie.getYear() %>
	</div>
	<!-- Casting (éventuel) -->
	<div>
		<%
			if (movie.getCasting() != null && !(movie.getCasting().isEmpty())) {
		%>
			<b>avec</b>
			<%
				Iterator<ActorBean> it = movie.getCasting().iterator();
					while (it.hasNext()) { 
				ActorBean acteur = it.next();
			%>
				<%=acteur.getName()%>
				<% if (StringUtils.isNotBlank(acteur.getRole())) { %>
					(<%= acteur.getRole() %>)<% if (it.hasNext()) { %>,<% } %>
				<% } %>
			<% } %>
		<% } %>
	</div>
	<!-- Notes presse/spectateurs -->
	<div>
		<span id="pressRating" style="display: hidden">
			<b>Note presse : </b>
			<img id="pressRatingContainer" class="ratingContainer" src="./images/computer/empty.gif"/>
			<input type="hidden" id="pressRatingValue" value="<%= movie.getPressRating() %>"/>
		</span>
		<span id="userRating" style="display: hidden">
			<b>Note spectateurs : </b>
			<img id="userRatingContainer" class="ratingContainer" src="./images/computer/empty.gif"/>
			<input type="hidden" id="userRatingValue" value="<%= movie.getUserRating() %>"/>
		</span>
	</div>
	<!-- Informations complémentaires -->
	<div>
		<% if (StringUtils.isNotBlank(movie.getOtherInfos())) { %>
			<div><i><%= movie.getOtherInfos() %></i></div>
		<% } %>
	</div>
	<!-- Genres -->
	<div>
		<b>Genre<% if (movie.getGenres().size() > 1) { %>s<% } %> : </b>
		<%= movie.getGenres().toString().substring(1, movie.getGenres().toString().length() - 1) %>
	</div>
	<!-- Nationalités -->
	<div>
		<b>Nationalit&eacute;<% if (movie.getNationalities().size() > 1) { %>s<% } %> : </b>
		<%= movie.getNationalities().toString().substring(1, movie.getNationalities().toString().length() - 1) %>
	</div>
	<!-- Synopsis -->
	<div>
		<b>Synopsis : </b>
		<%= movie.getSynopsis() %>
	</div>
	<!-- Lien vers la page du film sur AlloCine / TheMovieDb -->
	<div>
		<% if (MovieBean.Provider.ALLOCINE.equals(movie.getProvider())) { %>
			<a href="#" onclick="window.open('http://www.allocine.fr/film/fichefilm_gen_cfilm=' + $('#idMovie').val() + '.html');"><b>Page film sur AlloCine</b></a>
		<% } else if (MovieBean.Provider.THEMOVIEDB.equals(movie.getProvider())) { %>
			<a href="#" onclick="window.open('http://www.themoviedb.org/movie/' + $('#idMovie').val() + '?language=fr');"><b>Page film sur TheMovieDB</b></a>
		<% } %>
	</div>
	<!-- Lien vers la bande annonce -->
	<div>
		<a href="#" onclick="window.open($('#urlTrailer').val());"><b>Bande annonce</b></a>
	</div>
</div>