<%@page import="java.math.BigDecimal"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:useBean id="movieManager" class="com.fgougui.webmoviemanager.beans.MovieManagerBean" scope="application"/>
<%@ page import="com.fgougui.webmoviemanager.beans.MovieBean" %>

<!-- Titre -->
<h2>Liste des films</h2>

<% if (movieManager.getMovies().size() > 0) { %>

	<div id="choixTri">
		<input type="button" id="btnSortByGenre" value="Trier par genre" class="btnSort button selected" onclick="sortByGenre()"/>
		<input type="button" id="btnSortByFolder" value="Trier par dossier" class="btnSort button" onclick="sortByFolder()"/>
		<input type="button" id="btnSortByPopularity" value="Trier par classement" class="btnSort button" onclick="sortByPopularity()"/>
		<input type="text" id="searchField" value="" placeholder="Nom de film..." onkeydown="if (event.keyCode == 13) {searchMovies();};"/>
		<input type="button" id="btnSearch" value="Rechercher" class="button" onclick="searchMovies()"/>
		<span class="buttonSeparator">&nbsp;</span>
		<input type="button" id="btnDisplayIcons" value="Affichage en icônes" class="btnDisplay button selected" onclick="displayIcons()"/>
		<input type="button" id="btnDisplayList" value="Affichage en liste" class="btnDisplay button" onclick="displayList()"/>
		<span class="buttonSeparator">&nbsp;</span>
		<input type="button" id="btnShowAll" value="Tous les films" class="btnShow button selected" onclick="showAll()"/>
		<input type="button" id="btnShowToSee" value="Films à voir" class="btnShow button" onclick="showToSee()"/>
		<input type="button" id="btnShowNeverSeen" value="Films jamais vus" class="btnShow button" onclick="showNeverSeen()"/>
		<input type="button" id="btnShowAlreadySeen" value="Films déjà vus" class="btnShow button" onclick="showAlreadySeen()"/>
		<!-- 
		<span class="buttonSeparator">&nbsp;</span>
		<input type="button" id="btnUpdateAll" value="Tout actualiser" class="button" onclick="updateAll()"/>
		 -->
	</div>

	<!-- Liste des genres -->
	<div id="listGenres" class="list listSorting">
		<ul>
			<li id="genre_all" class="genre selected" onclick="filterByGenre('all')">
				Tous (<small class="counter"><%=movieManager.getMovies().size()%></small>)
			</li>
			<% for (String genre : movieManager.getGenres().keySet()) { %>
				<li id="genre_<%= genre.replaceAll(" ", "_") %>" class="genre" onclick="filterByGenre('<%= genre.replaceAll(" ", "_") %>')">
					<%= genre %> (<small class="counter"><%=movieManager.getGenres().get(genre)%></small>)
				</li>
			<% } %>
		</ul>
	</div>
	
	<!-- Liste des dossiers -->
	<div id="listFolders" class="list listSorting" style="display: none;">
		<ul>
			<li id="folder_all" class="folder selected" onclick="filterByFolder('all')">
				Tous (<small class="counter"><%=movieManager.getMovies().size()%></small>)
			</li>
			<%
				for (String folder : movieManager.getFolders().keySet()) {
			%>
				<li id="folder_<%= folder.replaceAll(" ", "_") %>" class="folder" onclick="filterByFolder('<%= folder.replaceAll(" ", "_") %>')">
					<%= folder %> (<small class="counter"><%=movieManager.getFolders().get(folder)%></small>)
				</li>
			<% } %>
		</ul>
	</div>
	
	<!-- Liste des moyennes des notes -->
	<div id="listPopularity" class="list listSorting" style="display: none;">
		<ul>
			<li id="popularity_all" class="popularity selected" onclick="filterByPopularity('all')">
				Tous (<small class="counter"><%=movieManager.getMovies().size()%></small>)
			</li>
			<li id="popularity_0_0" class="popularity" onclick="filterByPopularity('0_0')">
				Pas de critique disponible
			</li>
			<%
				for (float popularity : movieManager.getPopularities().keySet()) {
					String popularityString = StringUtils.replace(Float.toString(popularity), ".", "_");
					if (popularity != 0) {
				%>
					<li id="popularity_<%= popularityString %>" class="popularity" onclick="filterByPopularity('<%= popularityString %>')">
						<% BigDecimal popularityToDisplay = new BigDecimal(popularity); %>
						Entre <i><%= (popularityToDisplay.subtract(new BigDecimal("0.4"))) %></i> et <i><%= popularity %></i> (<small class="counter"><%=movieManager.getPopularities().get(popularity)%></small>)
					</li>
				<% }
			}%>
		</ul>
	</div>
	
	<!-- Liste des films -->
	<div id="listMovies" class="list iconStyle">
		<h3 id="searchResultsTitle" style="display: none;">
			R&eacute;sultats de la recherche pour "<span id="searchPattern"></span>"
		</h3>
		<div class="listUl">
			<%
				for (MovieBean movie : movieManager.getMovies()) {
			%>
				<div id="movie_<%= movie.getId() %>" class="listLi movie showMovie
															<% for(String genre : movie.getGenres()) { %>
																genre_<%= genre.replaceAll(" ", "_") %>
															<% } %>
															folder_<%= movie.getFolder().replaceAll(" ", "_")%>
															popularity_<%= StringUtils.replace(Float.toString(movie.getPopularityForSort()), ".", "_") %>
															<% if(movie.isToSee()) {%>
																toSee
															<% } %>
															<% if(movie.isAlreadySeen()) {%>
																alreadySeen
															<% } else { %>
																neverSeen
															<% } %>
															"
													onclick="loadMovieInfos(<%= movie.getId() %>)">
					<div class="poster">
						<%
							if (movie.getUrlPosterThumbnail() != null) {
						%>
							<img alt="poster" src="/WebMovieManager/Auth/Movie?action=execDisplayPosterThumbnail&idMovie=<%=movie.getId()%>" class="poster" id="posterMovieManager_<%=movie.getId()%>">
						<%
							} else {
						%>
							<img alt="poster" src="./images/computer/nopicture.jpg" class="poster" id="posterMovieManager_<%=movie.getId()%>">
						<%
							}
						%>
					</div>
					<div class="movieTitle"><%=movie.getTitle()%></div>
				</div>
			<% } %>
		</div>
	</div>
	
	<!-- Informations sur le film (rempli via appel AJAX) -->
	<div id="movieInfos" title="Informations sur le film" style="display: none;">
	</div>

<% } else { %>
	Aucun film dans la filmoth&egrave;que. Veuillez proc&eacute;der &agrave; une synchronisation.
<% } %>
