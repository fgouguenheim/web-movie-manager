<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@page import="java.util.Iterator"%>
<%@ page import="com.fgougui.webmoviemanager.beans.MovieBean" %>

<jsp:useBean id="movies" type="java.util.Collection" scope="request"/>

<%-- Liste des id des films en JSON --%>
<% if (movies.isEmpty()) { %>
	{"movies":null}
<% } else { %>
	{"movies":[
	<% Iterator<MovieBean> it = movies.iterator();
	while (it.hasNext()) {
		MovieBean movie = it.next(); %>
		<%= movie.getId() %>
		<% if (it.hasNext()) { %>
		,
		<% } %>
	<% } %>
	]}
<% } %>