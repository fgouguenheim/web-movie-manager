<%@ page language="java" contentType="text/xml; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:useBean id="message" class="java.lang.String" scope="request"/>

<exception><%= message %></exception>